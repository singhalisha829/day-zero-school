import Button from '@/components/Button'
import Input from '@/components/InputFields'
import { useState, useEffect } from 'react'
import JobCard from '@/components/JobCard'
import { MdCheckCircle, MdCheck, MdClose, MdCancel, MdEast, MdBookmarkBorder, MdArrowBack } from 'react-icons/md';
import { useMediaQuery } from 'react-responsive'
import Filters from '@/components/ComponentsForMobile/Filters'
import { PropagateLoader } from "react-spinners";
import { useTopBar } from '@/components/ComponentsForMobile/TopBarContext'
import FloatingIcon from '@/components/ComponentsForMobile/FloatingIcon';

export default function Home() {
  const [ctcRange,setCtcRange]= useState([2,80]);
  const [showFilterPanel,setShowFilterPanel] = useState(false)
  const [isLoading, setIsLoading] = useState(true);


  const isTab = useMediaQuery({ maxWidth: 1000 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  })
  const { setTopBarContent } = useTopBar();



  const tabItems = [
    {
      key: '1',
      label: `All Jobs`,
      children: `Content of Tab Pane 1`,
    },
    {
      key: '2',
      label: `Saved Jobs`,
      children: `Content of Tab Pane 2`,
    },
    {
      key: '3',
      label: `Applied Jobs`,
      children: `Content of Tab Pane 3`,
    },
  ];

  const jobDescriptionTabItems=[
    {
      key: '11',
      label: `Job Description`,
      children: `Content of Tab Pane 1`,
    },
    {
      key: '12',
      label: `Eligibility Criteria`,
      children: `Content of Tab Pane 2`,
    }
  ];

  const jobList = [{
    id:1,
    jobPosition:'Software Development Engineer',
    company:'Microsoft',
    department:'Information Technology',
    package:'CTC-10 lac/pa',
    grade:'CGPA 7.00',
    jobPosted:'20 Days Ago',
    location:'Bengaluru',
    isEligible:true,
    description:'',
    eligiblity:['Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla']
  },{
    id:2,
    jobPosition:'Front End Developer',
    company:'Microsoft',
    department:'Information Technology',
    package:'CTC-10 lac/pa',
    grade:'CGPA 7.00',
    jobPosted:'20 Days Ago',
    location:'Bengaluru',
    isEligible:true,
    description:'',
    eligiblity:['Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla']
  
  },{
    id:3,
    jobPosition:'Front End Developer',
    company:'Microsoft',
    department:'Information Technology',
    package:'CTC-10 lac/pa',
    grade:'CGPA 7.00',
    jobPosted:'20 Days Ago',
    location:'Bengaluru',
    isEligible:false,
    description:'',
    eligiblity:['Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla']
  
  },{
    id:4,
    jobPosition:'Front End Developer',
    company:'Microsoft',
    department:'Information Technology',
    package:'CTC-10 lac/pa',
    grade:'CGPA 7.00',
    jobPosted:'20 Days Ago',
    location:'Bengaluru',
    isEligible:false,
    description:'',
    eligiblity:['Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla']
  
  },{
    id:5,
    jobPosition:'Front End Developer',
    company:'Microsoft',
    department:'Information Technology',
    package:'CTC-10 lac/pa',
    grade:'CGPA 7.00',
    jobPosted:'20 Days Ago',
    location:'Bengaluru',
    isEligible:false,
    description:'',
    eligiblity:['Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla','Lorem ipsum dolor sit amet consectetur. Enim amet gravida quis nulla']
  
  }]

  const [selectedJob,setSelectedJob] = useState({});
  const [selectedTab,setSelectedTab] = useState(tabItems[0]);
  const [selectedJobDescTab,setSelectedJobDescTab] = useState(jobDescriptionTabItems[0]);

  useEffect(()=>{
    if(!isTab){
      setSelectedJob(jobList[0])
    }
    setIsLoading(false)
  },[])

  const onChange = (key) => {
    console.log(key);
  };

  if (isLoading && !showFilterPanel){
    return (
      <div className='row-span-full col-span-full flex justify-center items-center'>
        <PropagateLoader
          color='#7F56D9'
          loading={true}
          size={20}
          cssOverride={{top:isTab?'40vh':'-20px',left:'-20px'}}
          />
      </div>
    )}else if(showFilterPanel){
      return(
        <Filters filterList={[
          // {filterName:'Company',filterType:'single-select',filterKey:'company',filterData:companyList,optionName:'company_name',optionID:'company_id',defaultValue:selectedCompany},
          {filterName:'Date',filterType:'date',filterKey:'start_date',secondFilterKey:'end_date',defaultValue:undefined,secondFilterKeyDefaultValue:undefined}
      ]} onCloseFilter={()=>{setShowFilterPanel(false); setTopBarContent('');}}
      applyFilter={(filterParams)=>prepareFilterParamsBody(filterParams)}/>
      )
    }
  return (
    <div className={`w-full h-full overflow-hidden`}>
      {isTab?null:<div className='h-[5.5rem] flex items-center border-b-1 gap-6 px-2'>
        <Input type='text' className="w-[18%] h-[4rem]" label='Job Profile' placeholder='Enter Job Profile' />
        <Input type='text' className="w-[18%] h-[4rem]" label='Industry' placeholder='Your preferred Industry' />
        <Input type='text' className="w-[18%] h-[4rem]" label='Location' placeholder='Your preferred work location' />
        <Input type='switch' className="w-[18%] h-[4rem]" label='Remote Jobs' />
        <Input type='range' className="w-[18%] h-[4rem]" label='CTC' value={ctcRange} />

      </div>}
      
      <div className={`flex ${isTab?'flex-col ':''} w-full h-full`}>
      {(isTab && !selectedJob.id) || !isTab? <div className={`${isTab?'w-full h-[80rem] ':'border-r-1 w-[40%]'} overflow-auto relative`}>
        <span className={`flex justify-between bg-white sticky top-0 text-[16px] items-center h-[2.5rem] border-b-1`}>{tabItems.map(item=>(
          <span key={item.key} title={item.label} className={`flex cursor-pointer justify-center w-[33%] overflow-hidden text-ellipsis whitespace-nowrap h-full pt-2 ${selectedTab.key == item.key?'text-softviolet border-b-softviolet border-b-2 font-medium':'text-textgrey3 font-normal'}`} 
          onClick={()=>setSelectedTab(item)}>
            {item.label}</span>
        ))}</span>
        <div id='tab-content' >
          {selectedTab.label == 'All Jobs'?<>{jobList.map(job=>(<JobCard key={job.id} data={job} isSelected={job.id == selectedJob.id?true:false} onClick={()=>setSelectedJob(job)}/>))}</>:null}
          {selectedTab.label == 'Saved Jobs'?<>{jobList.map(job=>(<JobCard key={job.id} data={job} isSelected={job.id == selectedJob.id?true:false} onClick={()=>setSelectedJob(job)}/>))}</>:null}
          {selectedTab.label == 'Applied Jobs'?<>{jobList.map(job=>(<JobCard key={job.id} data={job} isSelected={job.id == selectedJob.id?true:false} onClick={()=>setSelectedJob(job)}/>))}</>:null}

        </div>
          {isTab?<FloatingIcon onClick={()=>{setShowFilterPanel(true); setTopBarContent('Filter');}}/>:null}
        </div>:null}
        {(isTab && selectedJob.id) || !isTab?<div className={isTab?'w-full h-full':'w-[60%]'}>
          {isTab?<span className='cursor-pointer flex text-lg my-[0.5rem] ml-2' onClick={()=>setSelectedJob({})}><MdArrowBack size={20} className='mr-1 mt-[3px]' />Job Profile</span>:null}
          <div className='h-[8rem] flex flex-col justify-between w-full'>
            <span className='flex p-4'>
              <span className='w-[20%]'>[Logo]</span>
              <span className={`${isTab?'w-[80%]':'w-[60%]'} flex flex-col`}>
                <p className='text-[18px] font-semibold'>{selectedJob.jobPosition}</p>
                <p className='text-[#98A2B3] text-[14px]'>{selectedJob.company} | {selectedJob.location} | {selectedJob.jobPosted}</p>
                </span>
              {isTab?null:<span className='w-[20%] flex justify-end'><span className={`border-1 w-fit rounded-md py-1 px-3 h-[2rem] flex items-center ${selectedJob.isEligible?'text-[#257E49]':'text-[#C40202]'}`}>
                {selectedJob.isEligible?<MdCheck className='mr-1'/>:<MdClose className='mr-1' />}{selectedJob.isEligible?'Eligible':'Not Eligible'}</span>
            </span>}

            </span>
          <span className='flex text-[16px] items-center h-[2.5rem] border-b-1 mb-[-2px]'>{jobDescriptionTabItems.map(item=>(
          <span key={item.key} className={`flex cursor-pointer justify-center ${isTab?'w-1/2':'w-[25%]'}  h-full overflow-hidden text-ellipsis whitespace-nowrap pt-2 
          ${selectedJobDescTab.key == item.key?'text-softviolet border-b-softviolet border-b-2 font-medium':'text-textgrey3 font-normal'}`} 
          onClick={()=>setSelectedJobDescTab(item)}>
            {item.label}</span>
        ))}</span>
          </div>

          <div className='h-full relative mb-0 py-2 px-4 overflow-y-auto'>
            {selectedJobDescTab.label == 'Job Description'?<div>
            <div className='my-2 text-[16px] font-semibold'>Job Overview</div>
            <div className='my-2 text-[16px] font-semibold'>About Company</div>
            </div>:null}
            {selectedJobDescTab.label == 'Eligibility Criteria'?<div>
              {selectedJob.isEligible?<p className='text-[#257E49] flex items-center mt-2'><MdCheckCircle className='mr-1'/>You are eligible for this Job profile</p>:<p className='text-[#C40202] flex items-center'><MdCancel className="mr-1"/>You are not eligible for this Job profile</p>}
              <div className='my-6 text-[16px] font-semibold'>Eligibility Criteria</div>
              <ul className='list-disc px-4'>{selectedJob.eligiblity.map(criteria=>(<li>{criteria}</li>))}</ul>

            </div>:null}


          </div>
          <div className={`flex items-center sticky bottom-0  ${isTab?'h-[4rem] shadow-top justify-around':'h-[5rem] border-t-1 justify-end'} px-3`}>
              <Button className=' flex text-[16px] items-center text-softviolet mr-4' size={isTab?'small':'medium'} variant='transparent'>Save Job <MdBookmarkBorder className='ml-1' /></Button>
              <Button className='flex text-[16px] items-center' size={isTab?'small':'medium'}>Apply Now <MdEast className='ml-1'/></Button>
          </div>
        </div>:null}
      </div>
               </div>

  )
}
