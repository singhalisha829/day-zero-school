import Button from "@/components/Button";
import Table from "@/components/Table";
import React from "react";
import Link from "next/link";
import { BsPencil, BsFilter } from "react-icons/bs";

//  Data from API
const data = [
  {
    job_id: "212",
    job_type: "Job",
    company_details: "Information Technology",
    location: "Bengaluru",
    job_name: "SDE II",
    deadline: "27/03/2023",
    willingness: (
      <Button variant="transparent" className={"font-medium text-sm"}>
        <span style={{ color: "#257E49" }}>Open</span>
      </Button>
    ),
    ctc: "₹12 Lakhs",
    actions: <BsPencil />,
  },
  {
    job_id: "212",
    job_type: "Job",
    company_details: "Information Technology",
    location: "Bengaluru",
    job_name: "SDE II",
    deadline: "27/03/2023",
    willingness: (
      <Button variant="transparent" className={"font-medium text-sm"}>
        <span style={{ color: "#257E49" }}>Open</span>
      </Button>
    ),
    ctc: "₹12 Lakhs",
    actions: <BsPencil />,
  },
  {
    job_id: "212",
    job_type: "Job",
    company_details: "Information Technology",
    location: "Bengaluru",
    job_name: "SDE II",
    deadline: "27/03/2023",
    willingness: (
      <Button variant="transparent" className={"font-medium text-sm"}>
        <span style={{ color: "#257E49" }}>Open</span>
      </Button>
    ),
    ctc: "₹12 Lakhs",
    actions: <BsPencil />,
  },
  {
    job_id: "212",
    job_type: "Job",
    company_details: "Information Technology",
    location: "Bengaluru",
    job_name: "SDE II",
    deadline: "27/03/2023",
    willingness: (
      <Button variant="transparent" className={"font-medium text-sm"}>
        <span style={{ color: "#257E49" }}>Open</span>
      </Button>
    ),
    ctc: "₹12 Lakhs",
    actions: <BsPencil />,
  },
  {
    job_id: "212",
    job_type: "Job",
    company_details: "Information Technology",
    location: "Bengaluru",
    job_name: "SDE II",
    deadline: "27/03/2023",
    willingness: (
      <Button variant="transparent" className={"font-medium text-sm"}>
        <span style={{ color: "#257E49" }}>Open</span>
      </Button>
    ),
    ctc: "₹12 Lakhs",
    actions: <BsPencil />,
  },
];

const PostedJobs = () => {
  const ColumnList = [
    {
      name: "Job ID",
      width: "7%",
      minWidth: "100px",
      key: "job_id",
      style: "flex",
    },
    {
      name: "Job Type",
      width: "10%",
      key: "job_type",
      style: "flex",
      minWidth: "80px",
    },
    {
      name: "Company Details",
      width: "15%",
      key: "company_details",
      secondRowKey: "location",
      style: "flex",
      minWidth: "160px",
    },
    {
      name: "Job Name",
      width: "7%",
      key: "job_name",
      style: "flex",
      minWidth: "110px",
    },
    {
      name: "Deadline",
      width: "10%",
      key: "deadline",
      style: "flex",
      minWidth: "110px",
    },
    {
      name: "Willingness",
      width: "20%",
      minWidth: "100px",
      key: "willingness",
      style: "flex",
      textAlign: "center",
    },
    {
      name: "CTC",
      width: "10%",
      minWidth: "110px",
      key: "ctc",
      style: "flex",
      textAlign: "center",
    },
    {
      name: "Actions",
      width: "7%",
      minWidth: "80px",
      key: "actions",
      // type: "dropdown",
      style: "flex",
      textAlign: "center",
    },
  ];
  const ColumnSortState = {
    job_id: "",
    company_details: "",
    job_name: "",
    deadline: "",
    willingness: "",
    ctc: "",
    actions: "",
  };

  return (
    <div className="py-10 px-20 max-xl:px-1">
      <div className="flex">
        <h1 className="font-medium text-2xl mb-10">Posted Jobs</h1>
        <div className="ml-auto flex gap-4">
          <Button className={"w-24 font-medium text-sm"}>
            <Link href="/admin/post-a-new-job">+ Add</Link>
          </Button>
          <Button variant={"transparent"} className={""}>
            <BsFilter className={"text-2xl text-black"} />
          </Button>
        </div>
      </div>
      <div className="w-full">
        <Table
          className="overflow-y-auto overflow-x-auto mx-4 h-fit"
          columns={ColumnList}
          isSortingNeeded
          isMultiRowSelectNeeded
          rowIdKey="job_id"
          rows={data}
          handleRowClick={(key, row) => {}}
          handleTableSort={(key, order) => {}}
          sortState={ColumnSortState}
        />
      </div>
    </div>
  );
};

export default PostedJobs;
