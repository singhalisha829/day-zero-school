import React, { useState } from "react";
import Image from "next/image";
import Button from "@/components/Button";
import { LiaRedoSolid, LiaUndoSolid } from "react-icons/lia";
import NewEmail from "@/components/NewEmail";

const EmailInbox = () => {
  const [showNewEmailContainer, setShowNewEmailContainer] = useState(false);
  const [showExpandedEmailContainer, setShowExpandedEmailContainer] =
    useState(false);
  const [expanedEmailContent, setExpanedEmailContent] = useState({
    image: "/Ellipse.png",
    name: "Sweta Sharma",
    to: "Nilesh Prem Prakash",
    subject:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum porro iusto molestiae at alias dignissimos quo voluptate quisquam atque delectus!",
    body: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus ut inventore architecto qui eaque ad.",
  });

  // select email card
  const emailClick = (e) => {
    // get all the email divs
    let emails = document.querySelectorAll("#email");
    // convert nodelist to array
    emails = [...emails];
    // removes class from all the elements
    emails.map((email) => email.classList.remove("bg-lightviolet"));
    //  add class to  only that specific element
    e.currentTarget.classList.add("bg-lightviolet");
    // show email container
    setShowExpandedEmailContainer(true);
  };

  const Email = ({ id }) => {
    return (
      <div
        id="email"
        className="flex border-b-1 p-4 cursor-pointer"
        onClick={(e) => emailClick(e)}
      >
        {/* left image */}
        <div className="w-[150px] my-auto">
          <Image
            id="email__image"
            src="/Ellipse.png"
            width={48}
            height={48}
            alt="Profile Picture"
          />
        </div>
        {/* right content */}
        <div>
          <div className="flex justify-between">
            <h3 id="email__name" className="text-lg font-semibold">
              Shweta Sharma
            </h3>
            <small
              id="email__time"
              className="font-medium text-xs italic text-gray-600"
            >
              3:17 PM
            </small>
          </div>
          <p
            id="email__course"
            className="text-gray-600 text-base font-medium py-1"
          >
            4th Year Mechanical Engg.
          </p>
          <p id="email__body" className="text-gray-600 text-xs">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
            similique ipsam nisi nihil assumenda libero dolores, non fuga
            ratione nulla.
          </p>
        </div>
      </div>
    );
  };
  const EmailExpanded = () => {
    return (
      <>
        <div className="border-b-1 py-4">
          <div className="flex">
            <Image
              src={expanedEmailContent.image}
              width={48}
              height={48}
              alt="Profile Picture"
              className="ripple"
            />
            <div className="pl-3">
              <h3 className="text-sm font-medium">Shweta Sharma</h3>
              <p className="text-sm font-medium text-gray-600">
                To {expanedEmailContent.to}
              </p>
            </div>
            <div className="flex gap-2 ml-auto">
              <LiaUndoSolid
                className="cursor-pointer text-gray-600"
                onClick={() => console.log("left clicked")}
              />
              <LiaRedoSolid
                className="cursor-pointer text-gray-600"
                onClick={() => console.log("right clicked")}
              />
            </div>
          </div>
          <p className="py-4 text-xl text-gray-900 ">
            {expanedEmailContent.subject}
          </p>
          <p className="text-base text-gray-900">{expanedEmailContent.body}</p>
        </div>
      </>
    );
  };

  const changeState = (state) => {
    setShowNewEmailContainer(state);
  };

  return (
    <div className="max-sm:flex-col-reverse flex h-[92vh] overflow-x-hidden overflow-y-clip">
      {/* Left Panel */}
      <div className="max-sm:w-[100%] w-[30%] border-r-1 overflow-y-auto">
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
        <Email />
      </div>
      {/* Right Panel */}
      <div className="max-sm:w-[100%] w-[70%] max-sm:h-fit pt-4 px-6 overflow-y-auto sm:overflow-y-auto">
        <div className="grid ml-auto mb-4">
          {!showNewEmailContainer && (
            <Button
              className={"px-6 justify-self-end"}
              onClick={() => setShowNewEmailContainer(true)}
            >
              New Mail
            </Button>
          )}
        </div>
        {showNewEmailContainer && (
          <NewEmail
            changeState={changeState}
            title={"New Mail"}
            showTemplateSelectDropdown={true}
          />
        )}
        {showExpandedEmailContainer && <EmailExpanded />}
      </div>
    </div>
  );
};

export default EmailInbox;
