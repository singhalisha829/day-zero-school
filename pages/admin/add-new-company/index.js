import Input from "@/components/InputFields";
import Link from "next/link";
import React, { useEffect } from "react";
import { useFormik } from "formik";
import { newCompanySchema } from "../../../schema";
import { HiArrowSmLeft } from "react-icons/hi";
import Button from "@/components/Button";
import { SingleSelectDropdown } from "@/components/Dropdown";
import toast, { Toaster, useToasterStore } from "react-hot-toast";

const AddNewCompany = () => {
  const { toasts } = useToasterStore();

  const formik = useFormik({
    initialValues: {
      company_name: "",
      founded_in: "",
      team_size: "",
      website: "",
      about_company: "",
    },
    validationSchema: newCompanySchema,
    onSubmit: () => {
      console.log("form is subbmitted");
    },
  });
  // display toast with the error message
  const notify = (err, attName) => {
    if (err[attName]) toast.error(err[attName]);
  };
  // limit toast to only one
  useEffect(() => {
    toasts
      .filter((t) => t.visible)
      .filter((_, i) => i >= 1)
      .forEach((t) => toast.dismiss(t.id));
  }, [toasts]);

  //  add image file
  function getImgData() {
    console.log("getImgData run");
    const chooseFile = document.getElementById("company__image");
    const imgPreview = document.getElementById("img__preview");

    const files = chooseFile.files[0];
    if (files) {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(files);
      fileReader.addEventListener("load", function () {
        imgPreview.style.display = "block";
        imgPreview.innerHTML = '<img src="' + this.result + '" />';
      });
    }
  }

  return (
    <div className="py-10 px-20 max-md:px-1 max-md:py-3 mx-auto">
      <Toaster />
      {/* Header info */}
      <h1 className="flex text-2xl font-semibold mb-6">
        <Link href={"/admin/company"}>
          <HiArrowSmLeft className="hover:cursor-pointer mt-1" />
        </Link>
        &nbsp;<span>Add New Company</span>
      </h1>
      <form action="">
        <div className="mr-10">
          <label className="text-xs leading-8">Company Logo*</label>
          <div className=" flex overflow-hidden relative w-fit">
            <button
              className="border-1 border-gray-200 bg-white py-2
               px-5 rounded-lg text-xs font-medium cursor-pointer"
            >
              Select file
            </button>
            <input
              id="company__image"
              type={"file"}
              name="myImage"
              accept="image/png, image/jpg, image/jpeg"
              style={{
                fontSize: "100px",
                position: "absolute",
                left: "0",
                top: "0",
                opacity: "0",
              }}
              onChange={() => getImgData()}
            />
          </div>
          <div id="img__preview" className="mt-4 w-16"></div>
        </div>
        <div className="flex max-md:block max-md:w-[100%] justify-between gap-10">
          <Input
            type={"text"}
            id={"company_name"}
            label={"Company Name*"}
            className={"max-md:w-[100%] w-[50%] h-[4.5rem] mt-3"}
            placeholder={"Name of the Organisation"}
            value={formik.values.job_title}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "company_name");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.company_name}
            formikTouched={formik.touched.company_name}
          />
          <Input
            type={"text"}
            id={"founded_in"}
            label={"Founded In"}
            className={"max-md:w-[100%] w-[50%] h-[4.5rem] mt-3"}
            placeholder={"Enter Founding Year"}
            value={formik.values.job_title}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "founded_in");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.founded_in}
            formikTouched={formik.touched.founded_in}
          />
        </div>
        <div className="flex max-md:block justify-between gap-10 mt-3">
          <Input
            type={"text"}
            id={"team_size"}
            label={"Team Size"}
            className={"max-md:w-[100%] h-[4.5rem] w-[50%] mt-1"}
            placeholder={"Example: 10-20"}
            value={formik.values.job_title}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "team_size");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.team_size}
            formikTouched={formik.touched.team_size}
          />
          <div className="w-[50%] max-md:w-[100%]">
            <SingleSelectDropdown
              dropdownLabel={"Industry*"}
              options={["Option 1", "Option 2", "Option 3", "option 4"]}
              optionName={3}
              activeOptionIndex={"1"}
              className={"h-[2.5rem] w-[fit]"}
              placeholder={"Select Industry*"}
            />
          </div>
        </div>
        <div className="flex justify-between gap-10 max-md:block">
          <Input
            id={"website"}
            type={"text"}
            label={"Website*"}
            className={"h-[4.5rem] w-[50%] max-md:w-[100%] mt-3"}
            placeholder={"Enter Company Website"}
            value={formik.values.website}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "website");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.website}
            formikTouched={formik.touched.website}
          />
          <div className="w-[50%]"></div>
        </div>
        <div className="mr-10 max-md:mr-0">
          <Input
            id={"about_company"}
            type={"textarea"}
            label={"About Company*"}
            rows="8"
            className="w-[50%] max-md:w-[100%] mt-3"
            placeholder={"Details about the Company"}
            value={formik.values.job_title}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "about_company");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.about_company}
            formikTouched={formik.touched.about_company}
          />
        </div>
        <Button className={"mt-5"}>Add Company</Button>
      </form>
    </div>
  );
};

export default AddNewCompany;
