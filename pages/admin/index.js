"use client";
import Button from "@/components/Button";
import { useState, useEffect } from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import Link from "next/link";

// Chart.js data
ChartJS.register(ArcElement, Tooltip, Legend);

const data = {
  datasets: [
    {
      data: [3, 3],
      backgroundColor: ["#7F56D9", "#EFE7FE"],
    },
  ],
};
const options = {
  plugins: {
    tooltip: {
      enabled: false,
    },
  },
  hover: { mode: null },
  responsive: true,
  elements: {
    arc: {
      borderWidth: 0,
    },
  },
  maintainAspectRatio: true,
};
const textCenter = {
  id: "textCenter",
  beforeDatasetsDraw(chart, args, pluginOptions) {
    const { ctx, data } = chart;
    ctx.save();
    ctx.font = "bolder 1.5rem '__Inter_20951f', '__Inter_Fallback_20951f'";
    ctx.fillStyle = "black";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(
      "50%", // insert mid doughnut value here
      chart.getDatasetMeta(0).data[0].x,
      chart.getDatasetMeta(0).data[0].y
    );
  },
};

export default function AdminDashboard() {
  const [isMobile, setIsMobile] = useState(false);
  const [isTab, setIsTab] = useState(false);
  const [isLaptop, setIsLaptop] = useState(false);

  //get the screen size
  useEffect(() => {
    if (window.innerWidth < 480) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }

    if (window.innerWidth < 769) {
      setIsTab(true);
    } else {
      setIsTab(false);
    }
  }, []);

  useEffect(() => {
    window.addEventListener("resize", () => {
      if (window.innerWidth < 480) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
    });
  });

  useEffect(() => {
    if (window.innerWidth < 1280) {
      setIsLaptop(true);
    } else {
      setIsLaptop(false);
    }
  });

  const [jobStats, setJobStats] = useState({
    jobsOffered: 500,
    jobsPosted: 500,
    internshipOffered: 500,
    internshipPosted: 500,
  });

  const [companyData, setCompanyData] = useState([
    {
      name: "Microsoft",
      jobsPosted: 25,
      jobsOffered: 8,
    },
    {
      name: "BNY Mellon",
      jobsPosted: 25,
      jobsOffered: 8,
    },
    {
      name: "Accenture",
      jobsPosted: 25,
      jobsOffered: 8,
    },
    {
      name: "Goldman Sachs",
      jobsPosted: 25,
      jobsOffered: 8,
    },
  ]);

  const StatsCardDesktop = () => {
    return (
      <div className="flex gap-4 mt-4">
        <div className="p-4 flex-1 rounded-xl border">
          <p className="text-xs">Job Offers</p>
          <h1 className="font-semibold text-2xl">{jobStats.jobsOffered}</h1>
        </div>
        <div className="p-4 flex-1 rounded-xl border">
          <p className="text-xs">Jobs Posted</p>
          <h1 className="font-semibold text-2xl">{jobStats.jobsPosted}</h1>
        </div>
        <div className="p-4 flex-1 rounded-xl border">
          <p className="text-xs">Internships Offered</p>
          <h1 className="font-semibold text-2xl">
            {jobStats.internshipOffered}
          </h1>
        </div>
        <div className="p-4 flex-1 rounded-xl border">
          <p className="text-xs">Internships Posted</p>
          <h1 className="font-semibold text-2xl">
            {jobStats.internshipPosted}
          </h1>
        </div>
      </div>
    );
  };

  const StatsCardMobile = () => {
    return (
      <div className="mt-4">
        <div className="flex gap-2 mb-3">
          <div className="w-[50%] p-4 rounded-xl border">
            <p className="text-xs">Job Offers</p>
            <h1 className="font-semibold text-2xl">{jobStats.jobsOffered}</h1>
          </div>
          <div className="w-[50%] p-4 rounded-xl border">
            <p className="text-xs">Jobs Posted</p>
            <h1 className="font-semibold text-2xl">{jobStats.jobsPosted}</h1>
          </div>
        </div>
        <div className="flex gap-2">
          <div className="w-[50%] p-4 rounded-xl border">
            <p className="text-xs">Internships Offered</p>
            <h1 className="font-semibold text-2xl">
              {jobStats.internshipOffered}
            </h1>
          </div>
          <div className="w-[50%] p-4 rounded-xl border">
            <p className="text-xs">Internships Posted</p>
            <h1 className="font-semibold text-2xl">
              {jobStats.internshipPosted}
            </h1>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="p-6 max-md:p-1">
      {/* Header */}
      <div className="flex justify-between">
        <h1 className="text-2xl font-medium">Dashboard</h1>
        <Button className="text-sm font-medium">
          <span className="text-sm font-medium">+</span>
          &nbsp;New Job
        </Button>
      </div>
      {/* Jobs/Int. posted stats */}
      {isMobile ? <StatsCardMobile /> : <StatsCardDesktop />}
      {/* chart and table */}
      <div className="xl:flex xl:gap-4 justify-between">
        {/* Placement Stats (left section) */}
        <div className="rounded-xl xl:w-[70%] max-xl:w-[100%] border mt-4 p-4">
          <div className="flex justify-between mb-4">
            <h3 className="font-medium text-xl">Placement Stats</h3>
            <p>
              <Link href={"/"} className="font-normal text-xs text-softviolet">
                View All Students &gt;
              </Link>
            </p>
          </div>
          {/* chart */}
          <div
            id="chart"
            className="lg:flex max-sm:flex-col max-sm:mx-auto lg:gap-10 justify-between px-2"
          >
            {/* Left Chart */}
            <div>
              <div className="flex max-xl:justify-around">
                <div
                  style={
                    isTab
                      ? { width: "50%" }
                      : isMobile
                      ? { width: "70%" }
                      : { width: "80%" }
                  }
                >
                  <Doughnut
                    data={data}
                    options={options}
                    plugins={[textCenter]}
                  ></Doughnut>
                </div>
                <div className="flex flex-col justify-around ml-4">
                  <div className="">
                    <h3 className="font-medium text-2xl">202</h3>
                    <p className="font-medium text-xs">Students Placed</p>
                  </div>
                  <div className="">
                    <h3 className="font-medium text-2xl">404</h3>
                    <p className="font-medium text-xs">Total Students </p>
                  </div>
                </div>
              </div>
              <div className="grid">
                <h3 className="justify-self-center font-semibold mt-2 text-base">
                  Batch of <span className="text-softviolet">24</span>
                </h3>
              </div>
            </div>
            {/* Right Chart */}
            <div>
              <div className="flex max-lg:mt-10 max-xl:justify-around">
                <div
                  style={
                    isTab
                      ? { width: "50%" }
                      : isMobile
                      ? { width: "70%" }
                      : { width: "80%" }
                  }
                >
                  <Doughnut
                    data={data}
                    options={options}
                    plugins={[textCenter]}
                  ></Doughnut>
                </div>
                <div className="flex flex-col justify-around ml-4">
                  <div>
                    <h3 className="font-medium text-2xl">202</h3>
                    <p className="font-medium text-xs">Students Placed</p>
                  </div>
                  <div>
                    <h3 className="font-medium text-2xl">404</h3>
                    <p className="font-medium text-xs">Total Students </p>
                  </div>
                </div>
              </div>
              <div className="grid">
                <h3 className="justify-self-center font-semibold mt-2 text-base">
                  Batch of <span className="text-softviolet">24</span>
                </h3>
              </div>
            </div>
          </div>
        </div>
        {/* Companies (right section) */}
        <div className="rounded-xl xl:w-[30%] max-xl:w-[70%] max-md:w-[100%] border mt-4 p-4">
          <div className="flex justify-between mb-4">
            <h3 className="font-medium text-xl">Companies</h3>
            <Link href={"/"} className="font-normal text-xs text-softviolet">
              View All &gt;
            </Link>
          </div>
          {/* Table */}
          <table class="table-auto flex flex-col">
            <thead>
              <tr className="flex justify-between text-gray-400 text-xs font-medium">
                <th
                  // style={{
                  //   minWidth: "50px",
                  // }}
                  className="px-1 text-left"
                >
                  Name
                </th>
                <th className="px-1 text-right">Jobs Posted</th>
                <th className="px-1 text-right">Jobs Offered</th>
              </tr>
            </thead>
            <tbody className="">
              {companyData.map((data) => {
                return (
                  <tr key={data} className="flex justify-between">
                    <td
                      style={{
                        minWidth: "50px",
                      }}
                      className="text-sm font-normal justify-between pt-4 pr-4 text-left flex-1"
                    >
                      {data.name}
                    </td>
                    <td className="text-sm font-normal justify-between pt-4 pr-4 text-left flex-1">
                      {data.jobsPosted}
                    </td>
                    <td className="text-sm font-normal justify-between pt-4 pr-4 text-left flex-1">
                      {data.jobsOffered}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <table />
        </div>
      </div>
    </div>
  );
}
