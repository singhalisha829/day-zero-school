import Button from "@/components/Button";
import NewEmail from "@/components/NewEmail";
import React, { useState } from "react";

// Data from API
const templates = [
  {
    id: 1,
    name: "Temmplate Name 1",
    subject:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, molestias.",
  },
  {
    id: 2,
    name: "Temmplate Name 2",
    subject:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, molestias.",
  },
  {
    id: 3,
    name: "Temmplate Name 3",
    subject:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, molestias.",
  },
  {
    id: 4,
    name: "Temmplate Name 4",
    subject:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt, molestias.",
  },
];

const EmailTemplate = () => {
  const [editTemplateFlag, setEditTemplateFlag] = useState(0);
  const [showNewEmailContainer, setShowNewEmailContainer] = useState(true);

  // Template Card component
  const TemplateCard = ({ id, name, subject }) => {
    const changeState = (state) => {
      setShowNewEmailContainer(state);
    };

    return (
      <>
        {editTemplateFlag === id && showNewEmailContainer ? (
          <NewEmail
            className="mt-3"
            changeState={changeState}
            title={name}
            subject={subject}
            showTemplateSelectDropdown={false}
          />
        ) : (
          <div id={id}>
            <div className="border-1 rounded-xl p-6 mt-3">
              <div className="flex justify-between mb-4">
                <h3 className="text-xl font-medium">{name} </h3>
                <div className="text-softviolet flex">
                  <p className="mx-2 text-base font-medium cursor-pointer">
                    View
                  </p>
                  <p
                    className="mx-2 text-base font-medium cursor-pointer"
                    onClick={() => {
                      setShowNewEmailContainer(true);
                      setEditTemplateFlag(id);
                    }}
                  >
                    Edit
                  </p>
                </div>
              </div>
              <p className="text-base font-normal text-gray-600">{subject}</p>
            </div>
          </div>
        )}
      </>
    );
  };

  return (
    <div className="py-10 px-20 max-sm:px-1 max-sm:py-2">
      {/* header */}
      <div className="flex justify-between  mb-10">
        <h1 className="font-medium text-2xl">Email Templates</h1>
        <Button className={"font-medium text-sm px-4"}>
          <span className="text-xl">+</span> New Template
        </Button>
      </div>
      {/* card */}
      {templates.map((template, key) => {
        return (
          <TemplateCard
            key={template.id}
            id={template.id}
            name={template.name}
            subject={template.subject}
          />
        );
      })}
    </div>
  );
};

export default EmailTemplate;
