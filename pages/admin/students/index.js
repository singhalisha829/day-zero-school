import React, { useState } from "react";
import Button from "@/components/Button";
import { SingleSelectDropdown } from "@/components/Dropdown";
import { BsDownload, BsFilter } from "react-icons/bs";
import { MdFilterListOff } from "react-icons/md";
import Table from "@/components/Table";

const dropdownOptions = [
  { name: "Selected", id: 1, color: "#257E49" },
  { name: "Not Selected", id: 2, color: "#DE0202" },
  { name: "Shortlisted", id: 3, color: "#F4BB44" },
];

// Data from API
const data = [
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Not Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Not Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
  {
    name: "Nilesh Prem Prakash",
    roll_no: "1928829",
    email: "npprakash05@gmail.com",
    mobile_no: "+91-7050685297",
    course: "IDD",
    branch: "MNC",
    cgpa: "7.80",
    placement_status: (
      <SingleSelectDropdown
        options={dropdownOptions}
        optionName="name"
        optionID="id"
        placeholder="Select Company"
        selected={"Selected"}
        className="rounded w-[75%] h-[30px] mt-5"
      />
    ),
    company: "Microsoft",
  },
];

const Students = () => {
  const [filterIsOn, setFilterIsOn] = useState(false);

  // Filter component
  const FilterOptionsList = () => {
    return (
      <div className="flex gap-4">
        <SingleSelectDropdown
          options={[
            { name: "Option 1", id: 1 },
            { name: "Option 2", id: 2 },
            { name: "Option 3", id: 3 },
          ]}
          optionName="name"
          optionID="id"
          className={
            "h-[2rem] w-[7rem] pl-6 pr-6 rounded-full border-softviolet placeholder:text-softviolet"
          }
          placeholder={"Year (2)"}
        />
        <SingleSelectDropdown
          options={[
            { name: "Option 1", id: 1 },
            { name: "Option 2", id: 2 },
            { name: "Option 3", id: 3 },
          ]}
          optionName="name"
          optionID="id"
          className={
            "h-[2rem] w-[8rem] pl-6 pr-6 rounded-full border-softviolet text-softviolet"
          }
          placeholder={"Branch (5)"}
        />
        <SingleSelectDropdown
          options={[
            { name: "Option 1", id: 1 },
            { name: "Option 2", id: 2 },
            { name: "Option 3", id: 3 },
          ]}
          optionName="name"
          optionID="id"
          className={"h-[2rem] w-[7rem] pl-6 pr-6 rounded-full"}
          placeholder={"CGPA"}
        />
        <SingleSelectDropdown
          options={[
            { name: "Option 1", id: 1 },
            { name: "Option 2", id: 2 },
            { name: "Option 3", id: 3 },
          ]}
          optionName="name"
          optionID="id"
          className={"h-[2rem] w-[12rem] pl-6 pr-6 rounded-full"}
          placeholder={"Placement Status"}
        />
      </div>
    );
  };

  const columnSortState = {
    basic_info: "",
    contact_info: "",
    course: "",
    branch: "",
    cgpa: "",
    placement_status: "",
    company: "",
  };

  const columnList = [
    {
      name: "Basic Info",
      width: "20%",
      minWidth: "160px",
      key: "name",
      secondRowKey: "roll_no",
      style: "flex",
    },
    {
      name: "Contact Info",
      minWidth: "160px",
      width: "20%",
      key: "email",
      secondRowKey: "mobile_no",
      style: "flex",
    },
    {
      name: "Course",
      minWidth: "160px",
      width: "7%",
      key: "course",
      style: "flex",
    },
    {
      name: "Branch",
      minWidth: "160px",
      width: "7%",
      key: "branch",
      style: "flex",
    },
    {
      name: "CGPA",
      minWidth: "160px",
      width: "7%",
      key: "cgpa",
      style: "flex",
    },
    {
      name: "Placement Status",
      minWidth: "160px",
      width: "15%",
      key: "placement_status",
      style: "flex",
    },
    {
      name: "Company",
      minWidth: "160px",
      width: "10%",
      key: "company",
      style: "flex",
    },
  ];

  return (
    <div className="py-10 px-20 max-xl:px-1">
      {/* header */}
      <div className="flex mb-2">
        <h1 className="text-2xl font-medium">Students</h1>
        <div className="flex gap-2 ml-auto">
          <Button variant={"transparent"} className={"flex gap-1 pt-2"}>
            <BsDownload className="text-xl text-black" />
            <span className="text-black mt-0.5 ml-1">CSV File</span>
          </Button>
          <Button
            variant={"transparent"}
            onClick={() => setFilterIsOn(!filterIsOn)}
          >
            {filterIsOn ? (
              <MdFilterListOff className="text-2xl text-gray-400" />
            ) : (
              <BsFilter className="text-2xl text-black" />
            )}
          </Button>
        </div>
      </div>
      {/* filters */}
      {filterIsOn ? <FilterOptionsList className="z-20" /> : <></>}
      {/* Table */}
      <Table
        className="overflow-y-auto overflow-x-auto h-fit mt-4"
        columns={columnList}
        rowIdKey="contact_info"
        rows={data}
        isSortingNeeded
        isMultiRowSelectNeeded
        handleRowClick={(key, row) => {}}
        handleTableSort={(key, order) => {}}
        sortState={columnSortState}
      />
    </div>
  );
};

export default Students;
