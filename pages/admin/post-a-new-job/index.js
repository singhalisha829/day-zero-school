import Link from "next/link";
import React, { useEffect } from "react";
import Button from "@/components/Button";
import Input from "@/components/InputFields";
import { useFormik } from "formik";
import { postNewJobchema } from "../../../schema";
import { HiArrowSmLeft } from "react-icons/hi";
import toast, { Toaster, useToasterStore } from "react-hot-toast";

export default function PostNewJob() {
  const { toasts } = useToasterStore();

  const formik = useFormik({
    initialValues: {
      job_title: "",
      industry: "",
      company: "",
      ctc: "",
      location: "",
      application_deadline: "",
      about_company: "",
      role_requirements: "",
      perks_benefits: "",
    },
    validationSchema: postNewJobchema,
    onSubmit: () => {
      console.log("form is subbmitted");
    },
  });

  // display toast with the error message
  const notify = (err, attName) => {
    if (err[attName]) toast.error(err[attName]);
  };
  // limit toast to only one
  useEffect(() => {
    toasts
      .filter((t) => t.visible)
      .filter((_, i) => i >= 1)
      .forEach((t) => toast.dismiss(t.id));
  }, [toasts]);

  // useEffect(() => {
  //   console.log(formik.values.about_company);
  //   // for (const key in formik.values) {
  //   //   console.log(`${key}: ${formik.errors[key]}`);
  //   // }
  // }, [formik.values]);

  return (
    <div className="py-10 px-20 max-sm:px-1 max-sm:py-3 mx-auto">
      <Toaster />
      {/* Header info */}
      <h1 className="flex text-2xl font-semibold">
        <Link href={"/admin/posted-jobs"}>
          <HiArrowSmLeft className="hover:cursor-pointer mt-1" />
        </Link>
        &nbsp;<span>Post New Job</span>
      </h1>
      {/* filter selection */}
      <div className="mt-5 mb-6 flex">
        <Input type={"checkbox"} checked="true" name="job" className="mr-2" />
        <label>Job</label>
        <Input
          type={"checkbox"}
          checked="true"
          name="internship"
          className="ml-10 mr-2"
        />
        <label>Internship</label>
      </div>
      {/* Form */}
      <form onSubmit={formik.handleSubmit}>
        {/* Part One */}
        <h3 className="text-xl my-4 font-semibold"> About Job</h3>
        {/* row 1 */}
        <div className="flex max-md:block justify-between max-md:w-[100%] gap-10">
          <Input
            className={`job_title w-[50%] max-md:w-[100%] h-[4rem]`}
            label={"Job Title*"}
            type={"text"}
            placeholder={"Expamle: Product Manager"}
            id={"job_title"}
            value={formik.values.job_title}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "job_title");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.job_title}
            formikTouched={formik.touched.job_title}
          />
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"Industry*"}
            placeholder="Select Industry"
            type={"text"}
            id={"industry"}
            value={formik.values.industry}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "industry");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.industry}
            formikTouched={formik.touched.industry}
          />
        </div>
        {/* row 2 */}
        <div className="flex max-md:block justify-between max-md:w-[100%] gap-10">
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"Select Company*"}
            placeholder="Name Of Organisation"
            type={"text"}
            id={"company"}
            value={formik.values.company}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "company");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.company}
            formikTouched={formik.touched.company}
          />
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"CTC*"}
            placeholder="Enter per annum CTC"
            id={"ctc"}
            type={"text"}
            value={formik.values.ctc}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "ctc");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.ctc}
            formikTouched={formik.touched.ctc}
          />
        </div>
        {/* row 3 */}
        <div className="flex max-md:block justify-between max-md:w-[100%]  gap-10">
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"Location*"}
            placeholder="Select Location"
            id={"location"}
            type={"text"}
            value={formik.values.location}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "location");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.location}
            formikTouched={formik.touched.location}
          />
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem] justify-between"
            label={"Application Deadline*"}
            placeholder={"Select Application Deadline"}
            type={"date"}
            id={"application_deadline"}
            value={formik.values.application_deadline}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "application_deadline");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.application_deadline}
            formikTouched={formik.touched.application_deadline}
          />
        </div>
        {/* row 4 */}
        <div className="flex mt-4">
          <Input
            type={"checkbox"}
            name="isRemote"
            checked="true"
            className="mr-2"
          />
          <label className="text-base">is remote</label>
        </div>
        {/* row 5 */}
        <div className="flex max-md:block justify-between max-sm:gap-3 gap-10">
          <Input
            className="w-[50%] max-md:w-[100%]"
            label={"About Company*"}
            type={"textarea"}
            placeholder="Details about the Company"
            rows={"8"}
            id={"about_company"}
            value={formik.values.about_company}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "about_company");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.about_company}
            formikTouched={formik.touched.about_company}
          />
          <Input
            className="w-[50%] max-md:w-[100%]"
            label={"Role Requirements*"}
            type={"textarea"}
            placeholder="Skills and Experience required for the role"
            rows={"8"}
            id={"role_requirements"}
            value={formik.values.role_requirements}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "role_requirements");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.role_requirements}
            formikTouched={formik.touched.role_requirements}
          />
        </div>
        {/* row 6 */}
        <div className="mr-10 max-md:mr-0">
          <Input
            className="w-[50%] max-md:w-[100%]"
            label={"Perks and Benefits*"}
            type={"textarea"}
            placeholder="Perks and benefits a selected candidate can look forward to"
            rows={"8"}
            id={"perks_benefits"}
            value={formik.values.perks_benefits}
            onChange={formik.handleChange}
            onBlur={(e) => {
              notify(formik.errors, "perks_benefits");
              formik.handleBlur(e);
            }}
            formikErr={formik.errors.perks_benefits}
            formikTouched={formik.touched.perks_benefits}
          />
        </div>
        {/* Part Two */}
        <h3 className="mt-6 font-semibold text-lg">Qualification</h3>
        <small className="text-gray-600 text-sm font-medium">
          Enter the eligibility criteria for the job
        </small>
        <br />
        <div className="flex mt-4">
          <Input type={"checkbox"} checked="true" for="openForAll" />
          <label className="text-sm pl-2">Open for All</label>
        </div>
        {/* row 1 */}
        <div className="flex max-md:block justify-between max-md:w-[100%] gap-10">
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"Branch"}
            type={"text"}
            placeholder="Select"
          />
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"CGPA"}
            type={"text"}
            placeholder="Select"
          />
        </div>
        {/* row 2 */}
        <div className="flex max-md:block max-md:w-[100%] mb-4 mr-10">
          <Input
            className="w-[50%] max-md:w-[100%] h-[4rem]"
            label={"Graduation Year*"}
            type={"text"}
            placeholder="Select"
          />
        </div>
        <Button type="submit">Post Job</Button>
      </form>
    </div>
  );
}
