import React from "react";
import Link from "next/link";
import Button from "@/components/Button";
import Table from "@/components/Table";
import { BsFilter, BsPencil } from "react-icons/bs";

// data from API
const data = [
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: <p className="flex">5</p>,
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
  {
    name: "Microsoft",
    industry: "Information Technology",
    jobs_posted: "22",
    jobs_offered: "5",
    actions: (
      <div className="flex gap-2 mt-5">
        <BsPencil />
        <span className="text-sm font-normal">Edit</span>
      </div>
    ),
  },
];

const columnSortState = {
  name: "",
  industry: "",
  jobs_posted: "",
  jobs_offered: "",
  actions: "",
};

const columnList = [
  {
    name: "Name",
    width: "25%",
    minWidth: "160px",
    key: "name",
    style: "flex",
  },
  {
    name: "Industry",
    width: "25%",
    minWidth: "160px",
    key: "industry",
    style: "flex",
  },
  {
    name: "Jobs Posted",
    width: "25%",
    minWidth: "160px",
    key: "jobs_posted",
    style: "flex",
  },
  {
    name: "Jobs Offered",
    width: "25%",
    minWidth: "160px",
    key: "jobs_offered",
    style: "flex",
  },
  {
    name: "Actions",
    width: "25%",
    minWidth: "160px",
    key: "actions",
    style: "flex",
  },
];

const Companies = () => {
  return (
    <div className="py-10 px-20 max-xl:px-1">
      {/* Header */}
      <div className="flex">
        <h1 className="font-medium text-2xl mb-6">Companies</h1>
        <div className="ml-auto flex gap-4">
          <Button className={"w-24 font-medium text-sm"}>
            <Link href="/admin/add-new-company">+ Add</Link>
          </Button>
          <Button variant={"transparent"} className={""}>
            <BsFilter className={"text-2xl text-black"} />
          </Button>
        </div>
      </div>
      {/* Table */}
      <Table
        className="overflow-y-auto overflow-x-visible h-fit mt-4 mx-4"
        columns={columnList}
        rowIdKey="contact_info"
        rows={data}
        isSortingNeeded
        isMultiRowSelectNeeded
        handleRowClick={(key, row) => {}}
        handleTableSort={(key, order) => {}}
        sortState={columnSortState}
      />
    </div>
  );
};

export default Companies;
