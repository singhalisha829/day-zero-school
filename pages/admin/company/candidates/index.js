import React from "react";
import Button from "@/components/Button";
import Table from "@/components/Table";
import { BsDownload, BsFilter } from "react-icons/bs";
import { MdOutlineMail } from "react-icons/md";

const CompanyCandidates = () => {
  const transactionsColumnList = [
    {
      name: "Roll No.",
      width: "10%",
      minWidth: "160px",
      key: "roll_no",
      style: "flex",
    },
    {
      name: "Name",
      width: "18%",
      minWidth: "160px",
      key: "name",
      style: "flex",
    },
    {
      name: "Contact Info",
      width: "18%",
      minWidth: "160px",
      key: "contact_info",
      style: "flex",
    },
    {
      name: "CGPA",
      width: "10%",
      minWidth: "60px",
      key: "CGPA",
      style: "flex",
    },
    {
      name: "Status",
      width: "18%",
      minWidth: "140px",
      key: "status",
      style: "flex",
    },
    {
      name: "Actions",
      width: "8%",
      minWidth: "140px",
      key: "actions",
      style: "flex",
    },
  ];

  const transactionsColumnSortState = {
    roll_no: "",
    name: "",
    contact_info: "",
    CGPA: "",
    status: "",
    actions: "",
  };

  const data = [
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span
            className={"px-3 hover:underline-offset-8"}
            style={{ color: "#4339F2", cursor: "pointer" }}
          >
            Applied
          </span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600  hover:underline cursor-pointer">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span style={{ color: "#257E49" }}>Shortlisted</span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600  hover:underline  cursor-pointer hover:underline-offset-2">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span style={{ color: "#257E49" }}>Shortlisted</span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600  hover:underline  cursor-pointer hover:underline-offset-2">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span style={{ color: "#257E49" }}>Shortlisted</span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600  hover:underline  cursor-pointer hover:underline-offset-2">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span style={{ color: "#257E49" }}>Shortlisted</span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600  hover:underline  cursor-pointer hover:underline-offset-2">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
    {
      roll_no: "1033540188881",
      name: "Nilesh Prem Prakash",
      contact_info: "hello@gmail.com 9209292909",
      CGPA: "8.00 ",
      status: (
        <Button variant="transparent" className={"font-medium text-sm"}>
          <span style={{ color: "#257E49" }}>Shortlisted</span>
        </Button>
      ),
      actions: (
        <div className="flex gap-2 pt-4 text-gray-600 cursor-pointer hover:underline">
          <BsDownload className="text-xl" />
          <p className="font-medium text-sm">Download</p>
        </div>
      ),
    },
  ];

  return (
    <div className="py-10 px-20 max-xl:px-1">
      {/* header */}
      <div className="flex mb-5">
        {/* heading */}
        <h1>
          <span className="text-lg font-medium text-gray-600">
            SDE II / Microsoft &gt;
          </span>
          &nbsp;
          <span className="text-2xl font-medium text-gray-900">Candidates</span>
        </h1>
        {/* button group */}
        <div className="ml-auto flex gap-2 text-gray-400 font-medium text-sm">
          <Button variant={"transparent"} className={"flex gap-1 pt-2"}>
            <MdOutlineMail className="text-xl text-gray-400" />
            <span className="text-gray-400">Mail</span>
          </Button>
          <Button variant={"transparent"} className={"flex gap-1 pt-2"}>
            <BsDownload className="text-xl text-gray-400" />
            <span className="text-gray-400">CSV</span>
          </Button>
          <Button variant={"transparent"}>
            <BsFilter className="text-2xl text-black" />
          </Button>
        </div>
      </div>

      {/* data table */}
      <Table
        className="overflow-y-auto overflow-x-visible mx-4 h-fit"
        columns={transactionsColumnList}
        isSortingNeeded
        isMultiRowSelectNeeded
        rowIdKey="job_id"
        rows={data}
        handleRowClick={(key, row) => {}}
        handleTableSort={(key, order) => {}}
        sortState={transactionsColumnSortState}
      />
    </div>
  );
};

export default CompanyCandidates;
