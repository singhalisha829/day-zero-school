'use client'

import Image from 'next/image'
import Button from '@/components/Button'
import Input from '@/components/InputFields'
import { useState } from 'react'
import LocalStorageService from '@/services/LocalStorageHandler'
import { useRouter } from 'next/router'

const localStorage = LocalStorageService.getService();

export default function Home() {
    const router = useRouter()
    const [formData, setFormData] = useState({
        user: '',
        username: '',
        password: ''
    })

    const onSubmit = () =>{
        if(formData.user){
            localStorage.setUser(formData)
            if(formData.user == 'admin'){
                router.push('/admin')
            }else{
                router.push('/')
            }
        }else{
            alert('Please Select User role!')
        }
    }
    return (
        <div className='w-[100vw] h-[100vh] flex relative'>
            <div className='w-1/2 h-full flex flex-col justify-center items-center'>
                <div className='w-[60%]'><p className=' text-[36px] mb-[1.5rem]'>Welcome Back</p>
                    <p className='text-[18px] text-textgrey3 font-light mb-2'>Are you a student or an administrator</p>
                    <Input type='radio'
                        className="w-full mb-2"
                        options={[{ name: 'Student', value: 'user' }, { name: 'Admin', value: 'admin' }]}
                        placeholder='Name of School'
                        value={formData.user}
                        onChange={(data) => setFormData({ ...formData, user: data })} />
                    <Input type='text'
                        className="w-full h-[4rem]"
                        label='Username'
                        placeholder='Your Username'
                        value={formData.username}
                        onChange={(data) => setFormData({ ...formData, username: data })} />
                    <Input type='password' 
                    className="w-full h-[4rem]" 
                    label='Password' 
                    placeholder='Your Password' 
                    value={formData.password}
                    onChange={(data)=>setFormData({...formData,password:data})}/>
        
        <Button className=' flex text-[12px] mt-[1rem] mx-auto items-center' size='small' 
        onClick={onSubmit}>Login</Button>

                </div>
                
                </div>
            <div className='w-1/2 h-full top-0 absolute right-0'>
                <Image
                    alt='login'
                    src="/login_img.png"
                    // width={500} 
                    // height={800}
                    fill
                    objectFit='cover'
                /></div>



        </div>

    )
}
