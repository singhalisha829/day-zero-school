import Image from 'next/image'
import Button from '@/components/Button'
import { useRouter } from 'next/navigation'
import { useMediaQuery } from 'react-responsive';

export default function Home() {
    const router=useRouter();
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
      // This callback is only executed on the client side
      return matches;
    })

  return (
    <div className='flex w-full h-full justify-center items-center'>
        <span className={`flex flex-col mt-[-2rem] ${isMobile?'':'ml-[-2rem]'}`}><Image alt="profile-not-completed" 
        src="/incomplete_task.png" width="250" height="250"  className='mx-auto'/>
        <span className={`text-softviolet ${isMobile?'text-xl mx-auto':'text-[36px]'} font-semibold mt-[2rem]`}>Your profile is not complete</span>
        <span className={`text-textgrey3 ${isMobile?'text-sm':'text-[18px]'} flex justify-center`}>Complete your profile to start applying to jobs</span>
        <Button className='w-[7rem] mx-auto mt-[2rem]' size={isMobile?'extrasmall':''} onClick={()=>router.push('/user/my-profile/personal-details')}>
            My Profile</Button></span>
        
               </div>

  )
}
