import Input from "@/components/InputFields"
import { useState } from "react";
import { SingleSelectDropdown } from "@/components/Dropdown";
import Button from "@/components/Button";
import { useMediaQuery } from 'react-responsive'

export default function Education() {
    const [profileStatus,setProfileStatus] = useState(15);
    const [personalDetails, setPersonalDetails] =useState({
        whatsapp:'',
        dob:'',
        linkedln:'',
        gender:{name:'',value:''},
        address:''
    })
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    const genderList =[{name:'Male',value:'male'},{name:'Female',value:'female'}]
      console.log(personalDetails)
    return(
        <div className="w-full h-full p-4">
             <span className="flex">
            <Input type='progress' value={profileStatus}/>
            <span className="flex flex-col ml-4 justify-center">
                <p className="text-[16px] font-medium">Profile status ({profileStatus}% complete)</p>
                {profileStatus != 100?<p className="text-[14px] text-textgrey2">Complete your profile to start applying for jobs</p>:null}
            </span>
        </span>
        <p className="text-[18px] my-6 font-medium">Personal Details</p>

        <span>
        <Input type='number' className={`${isMobile?'w-full':'w-[25rem]'} h-[4rem] mb-2`} label='Whatsapp Number' placeholder='Your whatsapp number' 
        value={personalDetails.whatsapp} 
        onChange={(value)=>setPersonalDetails({...personalDetails,whatsapp:value})}/>
        <Input type='date' className={`${isMobile?'w-full':'w-[25rem]'} h-[4rem] mb-2`} label='Date of Birth' placeholder='Your mobile number' 
        value={personalDetails.dob}
        onChange={(value)=>setPersonalDetails({...personalDetails,dob:value})}/>
        <Input type='text' className={`${isMobile?'w-full':'w-[25rem]'} h-[4rem] mb-2`} label='LinkedIn Profile' placeholder='Link to your linkedin profile' 
        value={personalDetails.linkedln}
        onChange={(value)=>setPersonalDetails({...personalDetails,linkedln:value})}/>
        <SingleSelectDropdown
                  padding='0.1rem'
                  options={genderList}
                  optionName='name'
                  optionID='value'
                  placeholder='Select'
                  label='Gender'
                  setselected={(name, id) => {
                    setPersonalDetails({...personalDetails,gender:{name:name,value:id}})
                  }}
                  selected={personalDetails.gender.name}
                  selectedOptionId={personalDetails.gender.value}
                  className={`rounded-md ${isMobile?'w-full':'w-[25rem]'} h-[4rem] mb-[5px]`}
                />        
        <Input type='text' className={`${isMobile?'w-full':'w-[25rem]'} h-[4rem] mb-2`} label='Address' placeholder='Your address' 
        value={personalDetails.address}
        onChange={(value)=>setPersonalDetails({...personalDetails,address:value})}/>

        </span>
        <span className={`flex mt-6 w-full gap-2 ${isMobile?'justify-center':''}`}>
        <Button className=' flex text-[12px] items-center' size='small' >Save Changes</Button>
        <Button className=' flex text-[12px] items-center text-softviolet' size='small' variant='transparent'>Clear</Button>

        </span>
        </div>
    )
}