import Input from "@/components/InputFields"
import { useState } from "react";
import Image from "next/image";
import DragNDrop from "@/components/DragNDrop";
import Button from "@/components/Button";
import { useMediaQuery } from 'react-responsive'

export default function ProfilePhoto() {
    const [profileStatus,setProfileStatus] = useState(15);
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    return(
        <div className="w-full h-full p-6">
              <span className="flex">
            <Input type='progress' value={profileStatus}/>
            <span className="flex flex-col ml-4 justify-center">
                <p className="text-[16px] font-medium">Profile status ({profileStatus}% complete)</p>
                {profileStatus != 100?<p className="text-[14px] text-textgrey2">Complete your profile to start applying for jobs</p>:null}
            </span>
        </span>
        <p className="text-[18px] my-6 font-medium">Update Profile Photo</p>
        <span className="">
            <Image src='/profile.png' width={600} height={150}/>
        </span>
        <div className={`h-[15rem] ${isMobile?'w-full':'w-[60%] '} mt-6`}><DragNDrop /></div>
        <span className={`flex mt-6 w-full gap-2 ${isMobile?'justify-center':''}`}>
        <Button className=' flex text-[12px] items-center' size='small' >Save</Button>
        <Button className=' flex text-[12px] items-center text-softviolet' size='small' variant='transparent'>Cancel</Button>

        </span>
        </div>
    )
}