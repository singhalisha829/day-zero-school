import Input from "@/components/InputFields"
import { useState,useEffect } from "react"
import { SingleSelectDropdown } from "@/components/Dropdown";
import { useMediaQuery } from 'react-responsive'
import { MdCancel } from "react-icons/md";

export default function Skills() {
    const [profileStatus,setProfileStatus] = useState(15);
    const [skillsList,setSkillsList] = useState([]);
    const [selectedSkills,setSelectedSkills] =useState([]);
    const [jobFunctionsList,setJobFunctionsList] = useState([]);
    const [selectedJobFunctions,setSelectedJobFunctions] =useState([]);
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    useEffect(()=>{
        const skillsArray = [
            { name: "Problem Solving", value: 1 },
            { name: "Programming Languages", value: 2 },
            { name: "Version Control (Git)", value: 3 },
            { name: "Web Development", value: 4 },
            { name: "Debugging and Testing", value: 5 },
            { name: "Database Management", value: 6 },
            { name: "Algorithm and Data Structures", value: 7 },
            { name: "Continuous Integration/Continuous Deployment (CI/CD)", value: 8 },
            { name: "Cloud Computing", value: 9 },
            { name: "Communication and Collaboration", value: 10 }
          ];
        setSkillsList(skillsArray)   
        setJobFunctionsList(skillsArray)       
    },[])

    const addNewSkills =(name,value) =>{
        let list = selectedSkills;
        list.push({name:name,value:value})
        setSelectedSkills([...list])

        let filteredSkillsList = skillsList.filter(skills=>skills.value != value);
        setSkillsList([...filteredSkillsList])
    }

    const removeSkills =(name,value) =>{
        let list = selectedSkills.filter(skills=>skills.value != value);
        setSelectedSkills([...list])

        let filteredSkillsList = skillsList;
        filteredSkillsList.push({name:name,value:value})
        setSkillsList([...filteredSkillsList])

       
    }

    const addJobFunctions =(name,value) =>{
        let list = selectedJobFunctions;
        list.push({name:name,value:value})
        setSelectedJobFunctions([...list])

        let filteredSkillsList = jobFunctionsList.filter(job=>job.value != value);
        setJobFunctionsList([...filteredSkillsList])
    }

    const removeJobFunctions =(name,value) =>{
        let list = selectedJobFunctions.filter(job=>job.value != value);
        setSelectedJobFunctions([...list])

        let filteredSkillsList = jobFunctionsList;
        filteredSkillsList.push({name:name,value:value})
        setJobFunctionsList([...filteredSkillsList])

       
    }

    return(
        <div className="w-full h-full p-4">
             <span className="flex">
            <Input type='progress' value={profileStatus}/>
            <span className="flex flex-col ml-4 justify-center">
                <p className="text-[16px] font-medium">Profile status ({profileStatus}% complete)</p>
                {profileStatus != 100?<p className="text-[14px] text-textgrey2">Complete your profile to start applying for jobs</p>:null}
            </span>
        </span>

        <p className="text-[18px] mt-6 font-medium">Skills</p>
        <p className="text-[14px] font-normal text-textgrey2">Skills will help customise jobs that suit for you</p>
        {selectedSkills.length > 0?<span className="flex gap-4 flex-wrap my-4 text-[12px] font-medium text-[#344054]">
            {selectedSkills.map(skill=>{
        return(<p className="bg-[#F2F4F7] flex rounded-full px-3 py-1" key={skill.value}>{skill.name} 
        <MdCancel size={16} className="ml-1 cursor-pointer" onClick={()=>removeSkills(skill.name,skill.value)}/></p>)})}</span>:null}
        <SingleSelectDropdown
                  padding='0.1rem'
                  options={skillsList}
                  optionName='name'
                  optionID='value'
                  placeholder='Add Skill'
                  setselected={(name, id) => addNewSkills(name,id)}
                //   selected={}
                //   selectedOptionId={}
                  className={`rounded-md ${isMobile?'w-full':'w-[25rem]'} h-[4rem] mt-2 mb-[5px]`}
                />  

        <p className="text-[18px] mt-6 font-medium">Job Function</p>
        <p className="text-[14px] font-normal text-textgrey2">Select job profiles that you are interested in</p>
        {selectedJobFunctions.length > 0?<span className="flex gap-4 flex-wrap my-4 text-[12px] font-medium text-[#344054]">
            {selectedJobFunctions.map(skill=>{
        return(<p className="bg-[#F2F4F7] flex rounded-full px-3 py-1" key={skill.value}>{skill.name} 
        <MdCancel size={16} className="ml-1 cursor-pointer" onClick={()=>removeJobFunctions(skill.name,skill.value)}/></p>)})}</span>:null}
        <SingleSelectDropdown
                  padding='0.1rem'
                  options={jobFunctionsList}
                  optionName='name'
                  optionID='value'
                  placeholder='Add Profile'
                  setselected={(name, id) => addJobFunctions(name,id)}
                //   selected={}
                //   selectedOptionId={}
                  className={`rounded-md ${isMobile?'w-full':'w-[25rem]'} h-[4rem] mt-2 mb-[5px]`}
                /> 

        <p className="text-[18px] mt-6 font-medium">Resume</p>
        <p className="text-[18px] mt-6 font-medium">Upload Resume</p>
        <p className="text-[14px] font-normal text-textgrey2">Make sure to upload an updated resume</p>


        </div>
    )
}