import Input from "@/components/InputFields"
import { useState } from "react"
import {MdOutlineMode} from "react-icons/md"
import GraduationForm from "@/components/GraduationForm";
import SeniorSecondaryForm from "@/components/SeniorSecondaryForm";
import SecondaryForm from "@/components/SecondaryForm";
import PostGraduationForm from "@/components/PostGraduationForm";

export default function Education() {
    const [profileStatus,setProfileStatus] = useState(15);
    const [editMode,setEditMode] = useState(null);
    const [addEducation,setAddEducation] = useState(null);
    const [graduationDetails, setGraduationDetails] = useState({
        college_name:'',
        start_year:'',
        end_year:'',
        degree:{name:'',id:''},
        stream:{name:'',id:''},
        cgpa:''
    })
    const [seniorSecondaryDetails, setSeniorSecondaryDetails] = useState({
        school_name:'',
        end_year:'',
        board:{name:'',id:''},
        performance_scale:{name:'',id:''},
        performance:''
    })
    const educationDetails={
        seniorSecondary:{
            stream:'Bachelor of Technology',
            institute:'Indian Institute of Technology (BHU) Varanasi',
            branch:'Mathematics and Computing',
            duration:'2019-2023'
        },
        graduation:{
            stream:'Bachelor of Technology',
            institute:'Indian Institute of Technology (BHU) Varanasi',
            branch:'Mathematics and Computing',
            duration:'2019-2023'
        }
    };

    console.log(addEducation,editMode)


    return(<div className="w-full h-full p-4">
        <span className="flex">
            <Input type='progress' value={profileStatus}/>
            <span className="flex flex-col ml-4 justify-center">
                <p className="text-[16px] font-medium">Profile status ({profileStatus}% complete)</p>
                {profileStatus != 100?<p className="text-[14px] text-textgrey2">Complete your profile to start applying for jobs</p>:null}
            </span>
        </span>
    {!(addEducation || editMode)?<><p className="text-[18px] mt-6 font-medium">Education</p>
    
    {!educationDetails.seniorSecondary?<div className="flex flex-col w-[80%] mt-4 text-[14px]">
        <p className="font-semibold flex justify-between">{educationDetails.seniorSecondary.stream} <MdOutlineMode className="cursor-pointer" size={20} onClick={()=>setEditMode('senior-secondary')} /></p>
        <p>{educationDetails.seniorSecondary.institute}</p>
        <p>{educationDetails.seniorSecondary.branch}</p>
        <p>{educationDetails.seniorSecondary.duration}</p>


    </div>:<p className="text-[16px] mt-4 font-semibold text-softviolet cursor-pointer" onClick={()=>setAddEducation('senior-secondary')}>+ Add Senior Secondary (XII)</p>}

    {educationDetails.secondary?<div className="flex flex-col w-[80%] mt-4 text-[14px]">
        <p className="font-semibold flex justify-between">{educationDetails.seniorSecondary.stream} <MdOutlineMode className="cursor-pointer" size={20} onClick={()=>setEditMode('secondary')} /></p>
        <p>{educationDetails.seniorSecondary.institute}</p>
        <p>{educationDetails.seniorSecondary.branch}</p>
        <p>{educationDetails.seniorSecondary.duration}</p>


    </div>:<p className="text-[16px] mt-4 font-semibold text-softviolet cursor-pointer" onClick={()=>setAddEducation('secondary')}>+ Add Secondary (XII)</p>}
    
    {educationDetails.graduation?<div className="flex flex-col w-[80%] mt-4 text-[14px]">
        <p className="font-semibold flex justify-between">{educationDetails.graduation.stream} <MdOutlineMode className="cursor-pointer" size={20} onClick={()=>setEditMode('graduation')} /></p>
        <p>{educationDetails.graduation.institute}</p>
        <p>{educationDetails.graduation.branch}</p>
        <p>{educationDetails.graduation.duration}</p>


    </div>:<p className="text-[16px] mt-4 font-semibold text-softviolet cursor-pointer" onClick={()=>setAddEducation('graduation')}>+ Add Graduation</p>}
    
    {!educationDetails.postGraduation?<div className="flex flex-col w-[80%] mt-4 text-[14px]">
        <p className="font-semibold flex justify-between">{educationDetails.seniorSecondary.stream} <MdOutlineMode className="cursor-pointer" size={20} onClick={()=>setEditMode('post-graduation')} /></p>
        <p>{educationDetails.seniorSecondary.institute}</p>
        <p>{educationDetails.seniorSecondary.branch}</p>
        <p>{educationDetails.seniorSecondary.duration}</p>


    </div>:<p className="text-[16px] mt-4 font-semibold text-softviolet cursor-pointer" onClick={()=>setAddEducation('post-graduation')}>+ Add Post Graduation</p>}</>:
    
    <div className="p-4">
        {addEducation == 'graduation'?<GraduationForm  formData={graduationDetails} 
         onChange={(name,id,key)=>setGraduationDetails({...graduationDetails,[key]:{name:name,id:id}})}  onCancel={()=>setAddEducation('')}/>
        :null}

        {addEducation == 'senior-secondary'?<SeniorSecondaryForm  formData={seniorSecondaryDetails} 
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})} onCancel={()=>setAddEducation('')} />
        :null}

        {addEducation == 'post-graduation'?<PostGraduationForm  formData={seniorSecondaryDetails} 
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})}  onCancel={()=>setAddEducation('')}/>
        :null}

        {addEducation == 'secondary'?<SecondaryForm  formData={seniorSecondaryDetails} 
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})} onCancel={()=>setAddEducation('')} />
        :null}

        {editMode == 'graduation'?<GraduationForm  formData={graduationDetails} isEditMode
         onChange={(name,id,key)=>setGraduationDetails({...graduationDetails,[key]:{name:name,id:id}})}  onCancel={()=>setEditMode('')}/>
        :null}

        {editMode == 'senior-secondary'?<SeniorSecondaryForm  formData={seniorSecondaryDetails} isEditMode
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})} onCancel={()=>setEditMode('')} />
        :null}

        {editMode == 'post-graduation'?<PostGraduationForm  formData={seniorSecondaryDetails} isEditMode
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})}  onCancel={()=>setEditMode('')}/>
        :null}

        {editMode == 'secondary'?<SecondaryForm  formData={seniorSecondaryDetails} isEditMode
         onChange={(name,id,key)=>setSeniorSecondaryDetails({...seniorSecondaryDetails,[key]:{name:name,id:id}})} onCancel={()=>setEditMode('')} />
        :null}

        </div>}

    </div>)
}