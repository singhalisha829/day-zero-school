import * as yup from "yup";

export const postNewJobchema = yup.object().shape({
  job_title: yup
    .string()
    .required("Please provide the Job Title")
    .min(3, "Job Title must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  industry: yup
    .string()
    .required("Industry Field is Required!")
    .min(3, "Industry must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25!"),
  company: yup
    .string()
    .required("Company is Required!")
    .min(3, "Company must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  ctc: yup
    .string()
    .required("CTC is Required!")
    .min(3, "CTC must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  location: yup
    .string()
    .required("Location is Required!")
    .min(3, "Location must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  application_deadline: yup
    .string()
    .required("Application Deadline is Required!")
    .min(3, "Application Deadline must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  about_company: yup
    .string()
    .required("About Company is Required!")
    .min(3, "Abiut Company must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  role_requirements: yup
    .string()
    .required("Role Requirements is Required!")
    .min(3, "Role Requirements must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  perks_benefits: yup
    .string()
    .required("Perks & Benefits is Required!")
    .min(3, "Perks & Benefits must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
});

export const newCompanySchema = yup.object().shape({
  company_name: yup
    .string()
    .required("Please provide the Comapny Name")
    .min(3, "Comapny Name must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  founded_in: yup
    .number()
    .required("Please provide the Founded In date")
    .min(3, "Founded In must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
  team_size: yup
    .number()
    .positive("Team Size must be greater than 0")
    .required("Please provide the Team Size"),
  website: yup
    .string()
    .required("Please provide the Website Link")
    .matches(
      /((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
      "Enter a valid URL!"
    ),
  about_company: yup
    .string()
    .required("Please provide the About Company")
    .min(3, "About Company must be atleast 3 characters long!")
    .max(25, "Max characters allowed is 25"),
});
