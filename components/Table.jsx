import React from "react";
import { useState } from "react";
import { FaArrowDown, FaArrowUp, FaEye } from "react-icons/fa";
import { TbArrowsUpDown } from "react-icons/tb";
import moment from "moment";
import Input from "./InputFields";
import { Pagination } from 'antd';
import { SingleSelectDropdown } from "./Dropdown";
import { dropdownData } from '@/components/dummyData'
import { useMediaQuery } from 'react-responsive'

const Table = ({
    rows,
    columns,
    sortState,
    isSortingNeeded,
    className,
    isMultiRowSelectNeeded,
    rowIdKey,
    ...props }) => {
    const [tableColumnSortState, setTableColumnSortState] = useState(        // array of columns that requires sorting 
        sortState
    );
    const [selectedRows, setSelectedRows] = useState({})
    const [selectedCompany, setSelectedCompany] = useState({ name: null, id: null });
    const isMobile = useMediaQuery({ maxWidth: 1000 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })


    // assign the table column sort direction according to the previous sort direction
    const handleTableSort = (key) => {
        if (tableColumnSortState[key] == "") {
            handleSortObject(key, "ASC");
        } else if (tableColumnSortState[key] == "ASC") {
            handleSortObject(key, "DSC");
        } else if (tableColumnSortState[key] == "DSC") {
            handleSortObject(key, "");
        }
    };


    // 
    const handleSortObject = (key, order) => {
        // clean the tableColumnSortState object as only one column can be sorted at a time.
        let newObject = Object.keys(tableColumnSortState).reduce((obj, key) => {
            obj[key] = "";
            return obj;
        }, {});
        newObject[key] = order;
        setTableColumnSortState(newObject);

        // code to handle sorting
        props.handleTableSort(key, order);
    };

    //   select/deselect all rows and trigger an event in parent component accordingly
    const handleCheckAll = (event) => {
        var idList = { ...selectedRows }

        if (event.target.checked) {
            for (const item of rows) {
                idList[item[rowIdKey]] = true;
            }
        } else {
            for (const item of rows) {
                idList[item[rowIdKey]] = false;
            }
        }

        console.log(idList)
        setSelectedRows({ ...idList })
    }

    //   select/deselect rows(based on row id) and trigger an event in parent component accordingly
    const handleCheckRow = (event, id) => {
        var idList = { ...selectedRows }
        if (event.target.checked) {
            idList[id] = true;
        } else {
            idList[id] = false;
        }
        console.log(idList)
        setSelectedRows({ ...idList })
    }

    return (
        <div className={`${className}`}>
            <table className="w-full border-collapse relative">

                {/* table header starts here */}
                <thead className="table-header">
                    <tr className={`bg-backgroundColor rounded-md ${isSortingNeeded ? 'h-[3rem]' : 'h-[2rem]'}`}>
                        {isMultiRowSelectNeeded ? 
                        <th className={`px-[1.1rem] w-[2rem] ${isMobile?'sticky left-0 bg-backgroundColor':''}`}><Input type="checkbox" onChange={(e) => handleCheckAll(e)} /></th> : null}
                        {columns
                            ? columns.map((column, index) => {
                                return (
                                    <th
                                        key={index}
                                        className={`${isSortingNeeded
                                                ? "hover:text-softviolet hover:cursor-pointer"
                                                : "cursor-default"
                                            } ${isMobile && index == 0?'sticky left-[3rem] bg-backgroundColor':''} ${index == 0 ? 'pl-2' : ''}`}
                                        style={{ width: column.width, minWidth: column.minWidth }}
                                        onClick={() => handleTableSort(column.key)}
                                    >
                                        <div
                                            className={`${column.style}   ${isMobile?'text-[14px]':'text-[16px]'} font-semibold employee-list-table-header`}
                                        >
                                            {column.name}

                                            {/* display sort icon based on the sort state of the column */}
                                            {isSortingNeeded?<span>{tableColumnSortState[column.key] == "" ? (
                                                <TbArrowsUpDown
                                                    size={19.5}
                                                    className="text-softviolet ml-1"
                                                />
                                            ) : null}
                                            {tableColumnSortState[column.key] == "ASC" ? (
                                                <FaArrowUp
                                                    size={18.5}
                                                    className="text-softviolet ml-1"
                                                />
                                            ) : (
                                                <span>
                                                    {tableColumnSortState[column.key] == "DSC" ? (
                                                        <FaArrowDown
                                                            size={18.5}
                                                            className="text-softviolet ml-1"
                                                        />
                                                    ) : null}
                                                </span>
                                            )}</span>:null}
                                        </div>
                                    </th>
                                );
                            })
                            : null}
                    </tr>
                </thead>

                {/* table body starts here */}
                <tbody className={`${isMobile?'text-[12px]':'text-[14px]'} font-light`}>
                    {rows
                        ? rows.map((row, rowIndex) => {
                            return (
                                <tr key={rowIndex} className={`border-b  `}>
                                    {isMultiRowSelectNeeded ? <td className={`px-[1.1rem]  ${isMobile?'sticky left-0 bg-white':''}`}><Input checked={selectedRows[row[rowIdKey]]} onChange={(e) => handleCheckRow(e, row[rowIdKey])} type="checkbox" /></td> : null}
                                    {columns
                                        ? columns.map((column, columnIndex) => {
                                            const commonStyle =`min-w-[${column.minWidth}] ${columnIndex == 0 ? 'pl-2' : ''} w-[${column.width}] ${isMobile && columnIndex == 0?'sticky left-[3rem] bg-white z-[100]':''} ${isMobile?'py-2':'py-4'}`;

                                            //   display table cells according to the type of column content(For example, if the column displays a date or a dropdown).
                                            if (column.type == "array") {
                                                return (
                                                    <td
                                                        key={columnIndex}
                                                        title={row[column.key].join(', ')}
                                                        className={`pr-4 whitespace-nowrap  text-${column.textAlign} ${commonStyle}
                                                        text-charlestongreen max-w-[1rem] text-ellipsis overflow-hidden `}
                                                    >
                                                        {row[column.key].join(', ')}
                                                    </td>
                                                );
                                            }

                                            else if (column.type == "date") {
                                                return (
                                                    <td
                                                        key={columnIndex}
                                                        className={`  ${commonStyle}`}
                                                        onClick={() => props.handleRowClick(column.key, row)}
                                                    >
                                                        {moment(row[column.key]).format("DD MMM, YYYY")}
                                                    </td>
                                                );
                                            }

                                            else if (column.type == "dropdown") {
                                                return (
                                                    <td
                                                        key={columnIndex}
                                                        className={`${commonStyle}
                                                        text-center align-middle `}
                                                    >
                                                        <SingleSelectDropdown
                                                            height='30px'
                                                            margin='0'
                                                            padding='0.25rem'
                                                            width='25rem'
                                                            options={dropdownData}
                                                            optionName='company_name'
                                                            optionID='company_id'
                                                            placeholder='Select Company'
                                                            setselected={(name, id) => {
                                                                { }
                                                            }}
                                                            selected={selectedCompany.name}
                                                            selectedOptionId={selectedCompany.id}
                                                        />
                                                    </td>
                                                );
                                            } else {
                                                return (
                                                    <td
                                                        key={columnIndex}
                                                        className={` ${column.tdClass} ${commonStyle}`}
                                                        onClick={() => props.handleRowClick(column.key, row)}
                                                    >
                                                        {column.secondRowKey?<span>{row[column.key]}<br/>{row[column.secondRowKey]}</span>:row[column.key]}
                                                    </td>
                                                );
                                            }
                                        })
                                        : null}
                                </tr>
                            );
                        })
                        : null}
                </tbody>
            </table>

            {/* pagination starts here */}
            <span className={`my-4 w-full flex justify-start ${isMobile?'sticky left-0':''}`}><Pagination
                showSizeChanger
                showTotal={(total) => `Rows per page: ${total}`}
                onChange={(page, pageSize) => { console.log(page, pageSize) }}        //do something on page number / page size change
                total={rows.length}
            /></span>
        </div>
    );
};

export default Table;
