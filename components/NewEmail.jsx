import React, { useState } from "react";
import Button from "@/components/Button";
import Input from "@/components/InputFields";
import { SingleSelectDropdown } from "@/components/Dropdown";
import { AiOutlinePlus } from "react-icons/ai";
import dynamic from "next/dynamic";
const ReactQuill = dynamic(import("react-quill"), { ssr: false });
import "react-quill/dist/quill.snow.css";

const NewEmail = ({
  changeState,
  title,
  subject,
  showTemplateSelectDropdown,
}) => {
  const [value, setValue] = useState("");
  const [subject1, setSubject1] = useState(subject);

  var toolbarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "code-block"],
    [{ header: [1, 2, 3, false] }],
    [{ list: "ordered" }, { list: "bullet" }],
    [{ script: "sub" }, { script: "super" }],
    [{ indent: "-1" }, { indent: "+1" }],
    ["image"],
    [{ color: [] }, { background: [] }],
    [{ align: [] }],
  ];
  return (
    <div>
      <div className="border rounded-md p-6 mt-3">
        <h3 className="font-medium text-xl mb-4">{title}</h3>
        <div
          className={`${!showTemplateSelectDropdown ? "hidden" : "block"}
          flex gap-2`}
        >
          <SingleSelectDropdown
            options={[
              {
                id: 1,
                name: "Option 1",
              },
              {
                id: 2,
                name: "Option 2",
              },
              {
                id: 3,
                name: "Option 3",
              },
            ]}
            optionName="name"
            optionID="id"
            placeholder="Select Template"
            className="rounded-md w-[10rem] h-[2.5rem] mb-[5px]"
            padding={"0 12px 0 10px"}
          />
          <AiOutlinePlus className="text-softviolet text-xl font-semibold mt-3.5 cursor-pointer ripple" />
        </div>
        <Input
          label="To*"
          type={"labelInsideInput"}
          className="h-[2rem] mt-4"
          placeholder={"Enter emails, seprated by commas"}
        />
        <Input
          label="Subject*"
          value={subject1}
          onChange={(e) => setSubject1(e.target.value)}
          type={"labelInsideInput"}
          className="h-[2rem] mt-4"
          placeholder={"Enter the Subject"}
        />
        <ReactQuill
          modules={{ toolbar: toolbarOptions }}
          theme="snow"
          value={value}
          placeholder="Enter your mail..."
          onChange={setValue}
          className="mt-4"
        />
        <br />
        <div className="flex justify-end gap-3">
          <Button
            variant={"transparent"}
            size={"small"}
            onClick={() => changeState(false)}
          >
            Cancel
          </Button>
          <Button size={"small"}>Send</Button>
        </div>
      </div>
    </div>
  );
};

export default NewEmail;
