import { MdLocationOn, MdCheck, MdClose } from 'react-icons/md';
import { useMediaQuery } from 'react-responsive';

const JobCard = (props) =>{
  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  })

return(<div className={`p-3 border-b-1 ${props.isSelected?'bg-[#F9F8F8]':''}`} onClick={()=>props.onClick()}>
            <span className='flex h-[3rem]'>
              <span className='w-[20%]'>Logo</span>
              <span className='w-[80%] flex flex-col'>
                <p className={`${isMobile?'text-[16px]':'text-[18px]'} font-semibold`}>{props.data.jobPosition}</p> 
                <p className={`${isMobile?'text-[14px]':'text-[15px]'} font-medium`}>{props.data.company}</p></span>
            </span>
            <span className={`flex  gap-2 ${isMobile?'text-[12px] h-[1.5rem] mt-4':'text-[14px] mt-6'}`}>
              <p className='bg-[#F5FBFF] flex items-center text-[#2181B8] px-2 rounded-sm'>{props.data.department}</p>
              <p className='bg-[#F5FBFF] flex items-center text-[#2181B8] px-2 rounded-sm'>{props.data.package}</p>
              <p className='bg-[#F5FBFF] flex items-center text-[#2181B8] px-2 rounded-sm'>{props.data.grade}</p>
            </span>
            <span className={`flex ${isMobile?'mt-4':'mt-6'} justify-between items-center`}>
              <span className={`flex gap-2 ${isMobile?'text-[12px]':'text-[14px]'} font-medium h-[1.5rem]`}>
                <p className='text-[#667085] bg-[#F9FAFB] rounded-sm px-2 flex items-center'>{props.data.jobPosted}</p>
                <p className='text-[#667085] bg-[#F9FAFB] rounded-sm px-2 flex items-center'><MdLocationOn size={17} />{props.data.location}</p>
              </span>
              <span className={`border-1 rounded-md py-1 px-3 ${isMobile?'text-[12px]':''} flex items-center ${props.data.isEligible?'text-[#257E49]':'text-[#C40202]'}`}>
                {props.data.isEligible?<MdCheck className='mr-1'/>:<MdClose className='mr-1' />}{props.data.isEligible?'Eligible':'Not Eligible'}</span>
            </span>
            </div>
)
}

export default JobCard;