import Input from "./InputFields"
import { SingleSelectDropdown } from "./Dropdown"
import { useState } from "react"
import { useMediaQuery } from "react-responsive"
import Button from "./Button"

const SecondaryForm = (props) =>{
    const [graduationDetails,setGraduationDetails] = useState(props.formData)
    const dummyData = [{name:'Option 1',id:1},{name:'Option 2',id:2},{name:'Option 3',id:3}]
    const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
        // This callback is only executed on the client side
        return matches;
      })

    return(
<div className={`${isMobile?'w-full':'w-[60%] min-w-[30rem]'} flex flex-col`}>
<p className="text-[18px] my-6 font-medium">{props.isEditMode?'Edit':'Add'} Secondary Details(X)</p>
        <Input type='text' className="w-full h-[4rem]" label='Name of School' placeholder='Name of School' />
        <span className={`w-full flex justify-between ${isMobile?'flex-col':''}`}>
        <Input type='year' className={`${isMobile?'w-full':'w-[47%]'} h-[4rem]`} label='Year of Completion' placeholder='Current Grade' />
        <SingleSelectDropdown
                  padding='0.25rem'
                  options={dummyData}
                  optionName='name'
                  optionID='id'
                  placeholder='Select'
                  setselected={(name, id) => {
                    props.onChange(name,id,'board');
                  }}
                  label='Board'
                  selected={graduationDetails.board.name}
                  selectedOptionId={graduationDetails.board.id}
                  className={`rounded-md ${isMobile?'w-full':'w-[47%]'} h-[4rem] mb-[5px]`}
                /></span>
        <span className={`w-full flex ${isMobile?'flex-col':''} justify-between`}>
        <SingleSelectDropdown
                  padding='0.25rem'
                  options={dummyData}
                  optionName='name'
                  optionID='id'
                  placeholder='Select'
                  label='Performance Scale'
                  setselected={(name, id) => {
                    props.onChange(name,id,'performance_scale');
                  }}
                  selected={graduationDetails.performance_scale.name}
                  selectedOptionId={graduationDetails.performance_scale.id}
                  className={`rounded-md ${isMobile?'w-full':'w-[47%]'} h-[4rem] mb-[5px]`}
                />
        <Input type='number' className={`${isMobile?'w-full':'w-[47%]'} h-[4rem]`} label='Performance' placeholder='Your score' />
                  </span>
        <span className={`flex mt-6 w-full gap-2 ${isMobile?'justify-center':''}`}>
        <Button className=' flex text-[12px] items-center text-softviolet' size='small' variant='transparent' onClick={()=>props.onCancel()}>Cancel</Button>
        <Button className=' flex text-[12px] items-center' size='small' >Save</Button>

        </span>
        </div>
    )
}

export default SecondaryForm;