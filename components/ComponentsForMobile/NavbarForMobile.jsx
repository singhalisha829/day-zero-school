import { BiMenu } from 'react-icons/bi';
import { useState } from 'react';
import { MdLogout, MdKeyboardArrowUp, MdKeyboardArrowDown, MdOutlineEdit } from 'react-icons/md'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation';
import { useTopBar } from "./TopBarContext";

const Navbar = ({ children, sidebarContent, userName }) => {
    const [isOpen, setIsOpen] = useState(false);
    const router = useRouter()
    const [showSubContent, setShowSubContent] = useState(false);
    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };
    const currentUrl = usePathname()
    const { topBarContent, triggerTopBarEvent } = useTopBar();



    return (
        <div className="wrapper">
            <div className={`${isOpen ? 'section' : ''}`}>
                <div className="flex items-center h-[3.5rem] p-2 border-b-1">
                {topBarContent == 'Filter'?<span className="w-full px-2 flex justify-between">
                        <span>Filters</span>
                        <button className='active:text-ornateorange' onTouchStart={()=>triggerTopBarEvent('Clear Filters')} 
                        onClick={()=>triggerTopBarEvent('Clear Filters')}
                        >Clear Filters</button></span>:<BiMenu size={30} className='hamburger cursor-pointer' onClick={()=>toggleMenu()} />}                </div>

                <div>

                    {/* Mobile menu */}
                    {isOpen ? <div className={`sidebar bg-[#F4F0FD] z-30 flex flex-col`}>
                    <div className='rounded-full border-1 h-[8rem] w-[8rem] mr-auto ml-auto relative'>
          <div className='z-1 flex justify-center items-center h-[2rem] w-[2rem] right-0 bg-white rounded-full border-1 absolute'>
            <MdOutlineEdit className='cursor-pointer' title='Edit Photo' onClick={()=>{router.push('/user/my-profile/profile-photo');setIsOpen(false)}}/>
          </div>
        </div>                        <p className='mx-auto mt-2 mb-6'>{userName}</p>

                        <nav>
                            <ul className='flex flex-col font-light'>
                                {sidebarContent.map((menuItem) => {
                                    return (
                                        <li key={menuItem.name}>
                                            <div className='px-3 py-1 pl-0'>
                                                <Link
                                                    className={`relative gap-1 rounded-md py-2 pr-2 items-center flex
                      ${currentUrl == menuItem.path ? 'font-medium bg-lightviolet text-violet' : ''} ml-2 w-[98%] pl-4 cursor-pointer`}
                                                    href={
                                                        menuItem.path
                                                    }
                                                    onClick={() => { menuItem.name === 'My Profile' ? setShowSubContent(!showSubContent) : setIsOpen(false) }}
                                                >
                                                    <span className='text-l mr-2'>
                                                        <Image alt={menuItem.name} src={currentUrl == menuItem.path ? menuItem.selectedTabIcon : menuItem.icon} width={25} height={25}
                                                        /></span>
                                                    <span className='flex justify-between w-full'
                                                    >
                                                        {menuItem.name}
                                                        {menuItem.name === 'My Profile' ? showSubContent ? <MdKeyboardArrowUp className='mt-[2px]' size={20} /> : <MdKeyboardArrowDown className='mt-[2px]' size={20} /> : null}

                                                        
                                                    </span>
                                                </Link>
                                                <div className='flex'>
                                                    {/* {showSubContent && menuItem.subMenu?<div className='w-[1px] border-1 ml-4 border-violet'></div>:null} */}
                                                            <ul className='flex flex-col font-light ml-9 text-[14px]'>
                                                                {showSubContent && menuItem.subMenu ? menuItem.subMenu.map((menuItem, listIndex) => {
                                                                    return (
                                                                        <li key={menuItem.name}>
                                                                            <div className='px-3'>
                                                                                <Link
                                                                                    className={`relative active:text-violet font-normal active:font-semibold sidebar_subcontents rounded-md py-2 pr-2 items-center flex ml-2 w-[98%] pl-4 cursor-pointer`}
                                                                                    href={
                                                                                        menuItem.path
                                                                                    }

                                                                                    onClick={()=>setIsOpen(false)}
                                                                                >
                                                                                    <span
                                                                                    >
                                                                                        {menuItem.name}
                                                                                    </span>
                                                                                </Link>
                                                                            </div>
                                                                        </li>
                                                                    )
                                                                }) : null}
                                                            </ul>
                                                        </div>
                                            </div>
                                        </li>
                                    )
                                })}
                            </ul>
                        </nav>

                        {/* Logout */}
                        <div className='flex items-center p-2 mx-auto mt-auto text-center rounded  w-[80%]'>
                            <span className='text-[14px] max-w-[120px] mr-4'>Logout</span>

                            <MdLogout
                                className='cursor-pointer text-ornateorange'
                                title='Logout'
                                size={20}
                                onClick={() => logout()}
                            />
                        </div>
                    </div> : null}
                </div>
                {/* children pages */}
                <section className={`${isOpen ? 'left-[15rem] blur-sm' : ''} h-[calc(100vh-3rem)] overflow-scroll`}>
                    {children}</section>
            </div>

        </div>
    )
}

export default Navbar;