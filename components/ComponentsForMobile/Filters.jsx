import { useState, useEffect } from "react"
import { useTopBar } from "./TopBarContext";
import { MdOutlineCheck, MdSearch, MdOutlineCancel } from 'react-icons/md';
import ModalDatePicker from "./ModalDatePicker";
import moment from "moment";
import LocalStorageService from "@/services/LocalStorageHandler";

const localStorage = LocalStorageService.getService();

const Filters = (props) => {
    const [selectedFilter, setSelectedFilter] = useState(props.filterList[0].filterKey);
    const [selectedFilterObject, setSelectedFilterObject] = useState({});
    const [appliedFilter, setAppliedFilter] = useState([]);
    const { topBarEvent, triggerTopBarEvent } = useTopBar();
    const [isOpenDateModal, setIsDateModalOpen] = useState(false);
    const [seacrhString, setSearchString] = useState('');
    const [dateFieldName, setDateFieldName] = useState('')
    let filterHtmlContent ;
    const currentDate = new Date()
    const oneYearBefore = new Date()
    oneYearBefore.setFullYear(currentDate.getFullYear() - 1)


    useEffect(() => {
        let list = []
        props.filterList.map(filter => {
            if (filter.defaultValue) {
                if (typeof filter.defaultValue == 'object' && filter.defaultValue.length <= 0) { }
                else {
                    list.push({ filterName: filter.filterKey, selectedOptions: filter.defaultValue })
                }
            }

            if (filter.secondFilterKeyDefaultValue) {
                list.push({ filterName: filter.secondFilterKey, selectedOptions: filter.secondFilterKeyDefaultValue })
            }
        })
    setAppliedFilter([...list])
    }, [])

    useEffect(() => {
        const index = props.filterList.findIndex(item => item.filterKey === selectedFilter);
        setSelectedFilterObject(props.filterList[index])
    }, [selectedFilter])

    useEffect(() => {
        const selectedCompanyInLocal = null;

        if (topBarEvent === 'Clear Filters' && (selectedCompanyInLocal != null || selectedCompanyInLocal != undefined)) {
            setAppliedFilter([{ filterName: 'start_date', selectedOptions: oneYearBefore },
            { filterName: 'end_date', selectedOptions: currentDate },{filterName:'company',selectedOptions:{company_name: selectedCompanyInLocal.company_name, company_id: selectedCompanyInLocal.company_id}}])
            triggerTopBarEvent('')
        }
    }, [topBarEvent])

    const selectAllItems = (e, selectedFilter) => {
        const filterList = appliedFilter;
        const index = filterList.findIndex(item => item.filterName === selectedFilter.filterKey);
        if (e.target.checked) {
            if (index == -1)
                filterList.push({ filterName: selectedFilterObject.filterKey, selectedOptions: selectedFilter.filterData, isSelectAll: true })
            else {
                filterList[index].selectedOptions = selectedFilter.filterData;
                filterList[index].isSelectAll = true;
            }

        } else {
            filterList.splice(index, 1)
        }
        setAppliedFilter([...filterList])

    }

    const handleItemSelect = (e, selectedItem, selectedFilter) => {
        const filterList = appliedFilter;
        const index = filterList.findIndex(item => item.filterName === selectedFilter.filterKey);
        if (e.target.checked) {
            if (index == -1) {
                filterList.push({ filterName: selectedFilterObject.filterKey, selectedOptions: [selectedItem], isSelectAll: false })
            } else {
                filterList[index].selectedOptions.push(selectedItem)
            }
            setAppliedFilter([...filterList])
        } else {
            const list = filterList[index].selectedOptions.filter(item => item[selectedFilter.optionID] !== selectedItem[selectedFilter.optionID])
            filterList.splice(index, 1)
            if (list.length > 0) {
                setAppliedFilter([...filterList, { filterName: selectedFilter.filterKey, selectedOptions: list, isSelectAll: false }])
            } else {
                setAppliedFilter([...filterList])
            }
        }
    }

    const handleSingleItemSelect =(e, selectedItem, selectedFilter) =>{
        const filterList = appliedFilter;
        const index = filterList.findIndex(item => item.filterName === selectedFilter.filterKey);
        if (e.target.checked) {
            if (index == -1) {
                filterList.push({ filterName: selectedFilterObject.filterKey, selectedOptions: selectedItem })
            } else {
                filterList[index].selectedOptions = selectedItem;
            }
            setAppliedFilter([...filterList])
        } else {
            
        } 
    }

    const searchFilterOptions = (searchText, selectedItem) =>{
        setSearchString(searchText)
        const index = props.filterList.findIndex(filter=>filter.filterKey == selectedItem.filterKey)
        const list = props.filterList[index].filterData.filter(o => Object.keys(o).some(
            k => String(o[k]).toLowerCase().includes(searchText.toLowerCase()))
          );
      
          // console.log(list)
      
          setSelectedFilterObject({...selectedFilterObject,filterData:list})
    }

    if (selectedFilterObject.filterType == 'date') {
        const index1 = appliedFilter.findIndex(e => e.filterName == selectedFilterObject.filterKey)
        const index2 = appliedFilter.findIndex(e => e.filterName == selectedFilterObject.secondFilterKey)

        filterHtmlContent = (<><span className="p-2 flex items-center">From: <span className='rounded-md flex items-center px-2 border-1 h-[1.8rem] ml-2 w-[60%]' onClick={() => { setIsDateModalOpen(true); setDateFieldName(selectedFilterObject.filterKey) }} >{index1 != -1 ? moment(appliedFilter[index1]?.selectedOptions).format("DD MMMM, YYYY") : null}</span></span>
            <span className="p-2 flex items-center">To: <span type='text' className='rounded-md flex items-center px-2 border-1 h-[1.8rem] ml-2 w-[60%]' onClick={() => { setIsDateModalOpen(true); setDateFieldName(selectedFilterObject.secondFilterKey) }} >{index2 != -1 ? moment(appliedFilter[index2]?.selectedOptions).format("DD MMMM, YYYY") : null}</span></span></>)
    } else {
        let singleSelectListSelectedOptionFound=false;
        const index = appliedFilter.findIndex(e=>e.filterName == selectedFilterObject.filterKey);
        const isSelectAll = appliedFilter[index]?.isSelectAll;
        // console.log(selectedFilterObject,appliedFilter,index,isSelectAll)
        filterHtmlContent = (<>
        <span className="w-full fixed bg-white"><li className=' h-[2rem] pl-3 p-1 flex text-sm'>
            <MdSearch size={18} className="mt-[3px] absolute text-[#ABBBC2]"/><input type="text" className="mr-1 w-[60%] border-b-1 focus-visible:outline-none pl-6" onChange={(e) => searchFilterOptions(e.target.value, selectedFilterObject)} />
            {seacrhString != ''?<MdOutlineCancel size={18} className="text-[#ABBBC2]" onClick={()=>searchFilterOptions('',selectedFilterObject)}/>:null}</li>
        {selectedFilterObject.filterType == 'single-select'?null:<li className=' h-[2rem] pl-3 py-1 flex text-sm '><input type="checkbox" className="mr-1" checked={isSelectAll} onChange={(e) => selectAllItems(e, selectedFilterObject)} /><span className="font-semibold mr-1">Select All </span>[{selectedFilterObject.filterData?.length}]</li>}</span>
        <ul className={`flex flex-col font-light ${selectedFilterObject.filterType == 'single-select'?'mt-[2.2rem]':'mt-[4rem]'}`}>
                {selectedFilterObject.filterData?.length > 0 ? selectedFilterObject.filterData.map((filter, filterIndex) => {
                    const selectedFilter = appliedFilter.filter(filter => filter.filterName == selectedFilterObject.filterKey);
                    let isSelected = false;
                   
                    // for multi-select filter filter, check if the option is selected 
                    if (selectedFilter.length > 0 && selectedFilterObject.filterType!='single-select') {
                        isSelected = selectedFilter[0].selectedOptions.findIndex(option => option[selectedFilterObject.optionID] == filter[selectedFilterObject.optionID]) != -1 ? true : false;

                    }
                    // for single-select filter filter, check if the option is selected only uptil the selected option is not found
                    else if(selectedFilterObject.filterType=='single-select' && !singleSelectListSelectedOptionFound){
                        isSelected = selectedFilter[0].selectedOptions[selectedFilterObject.optionID] == filter[selectedFilterObject.optionID]? true : false;
                    }

                    return (
                        <li className='pl-3 py-1 flex text-base font-medium' key={filterIndex}>
                            <input type="checkbox" checked={isSelected}
                                className="mr-1" onChange={(e) =>{selectedFilterObject.filterType=='single-select'?handleSingleItemSelect(e, filter, selectedFilterObject):handleItemSelect(e, filter, selectedFilterObject)} } />
                            {filter[selectedFilterObject.optionName]}</li>
                    )
                }) : null}
            </ul></>)
    }



    const handleDateChange = (date) => {
        const filterList = appliedFilter;
        const index = filterList.findIndex(item => item.filterName === dateFieldName);
        if (index == -1) {
            filterList.push({ filterName: dateFieldName, selectedOptions: date })
        } else {
            filterList[index].selectedOptions = date;
        }
        setAppliedFilter([...filterList])
    }



    return (
        <>
            <div className="flex w-full rounded-md relative ">
                <nav className="h-[calc(100vh-3rem)] bg-lightviolet border-r-1 w-[30%]">
                    <ul className='flex flex-col font-light'>
                        {props.filterList ? props.filterList.map((filter, filterIndex) => {
                            const index = appliedFilter.findIndex(item => item.filterName == filter.filterKey);
                            const hasSelectedItems = index != -1 ? true : false;
                            const noOfAppliedFilters = hasSelectedItems ? appliedFilter[index].selectedOptions.length : 0;
                            return (
                                <li className={`pl-3 py-2 flex text-base font-medium ${filter.filterKey == selectedFilter ? 'bg-white' : ''}`} key={filterIndex}
                                    onClick={() => setSelectedFilter(filter.filterKey)}>{filter.filterName} {hasSelectedItems ? <span >{(filter.filterType=='date' || filter.filterType == 'single-select')?<MdOutlineCheck className="text-softviolet ml-1 mt-1"/>:<span className="text-softviolet text-[0.7rem] ml-1">({ noOfAppliedFilters })</span>}</span> : null}</li>
                            )
                        }) : null}
                    </ul>
                </nav>
                <nav className="h-[85vh] w-[70%] overflow-scroll">
                    {filterHtmlContent}
                </nav>
            </div>
            <div className="h-[3rem] border-t-1 w-full flex items-center fixed bottom-0 bg-white">
                <span className="w-1/2 text-center" onClick={() => { props.onCloseFilter() }}>CLOSE</span>
                <span className="w-[0.5px] border-r-1 h-[2rem]"></span>
                <span className="w-1/2 text-center text-softviolet" onClick={() => {props.applyFilter(appliedFilter) }}>APPLY</span>
            </div>
            <ModalDatePicker isOpen={isOpenDateModal} onDateSelect={(date) => handleDateChange(date)} onClose={() => setIsDateModalOpen(false)} />

        </>
    )

}

export default Filters;