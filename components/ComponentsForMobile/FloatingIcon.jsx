import { MdOutlineFilterAlt } from 'react-icons/md'

const FloatingIcon = (props) =>{
    return(
<span className='fixed cursor-pointer z-10 flex justify-center items-center bottom-[5rem] right-[2rem] bg-softviolet h-[3rem] w-[3rem] rounded-full shadow-2xl' onClick={()=>props.onClick()}><MdOutlineFilterAlt size={25} className='text-white'/></span>
    )
}

export default FloatingIcon;