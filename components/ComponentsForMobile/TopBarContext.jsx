// TopBarContext.js
import { createContext, useContext, useState } from 'react';

const TopBarContext = createContext();

export const TopBarProvider = ({ children }) => {
  const [topBarContent, setTopBarContent] = useState('');
  const [topBarEvent, setTopBarEvent] = useState(null);

  const triggerTopBarEvent = (eventData) => {
    setTopBarEvent(eventData);
  };
  
  return (
    <TopBarContext.Provider value={{ topBarContent, setTopBarContent, topBarEvent, triggerTopBarEvent  }}>
      {children}
    </TopBarContext.Provider>
  );
};

export const useTopBar = () => {
  return useContext(TopBarContext);
};
