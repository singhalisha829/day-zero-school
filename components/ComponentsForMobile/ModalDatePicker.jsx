import React, { useState } from 'react';
import Calendar from 'react-calendar';
import { Modal } from 'antd';

const ModalDatePicker = (props) => {
  const [selectedDate, setSelectedDate] = useState(null);
  const customOkButtonProps = {
    style:{
        background:'#7F56D9'
    }
    
  };
  return (
    <Modal
    open={props.isOpen} onCancel={()=>props.onClose()}
    centered okText="Apply"
    onOk={()=>{props.onClose();props.onDateSelect(selectedDate)}}
    okButtonProps={customOkButtonProps}
    >
    <Calendar onChange={(e)=>{setSelectedDate(e)}} value={selectedDate} />
    </Modal>
  );
};

export default ModalDatePicker;
