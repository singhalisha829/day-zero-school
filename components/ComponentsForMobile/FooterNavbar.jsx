import { Badge } from 'antd';
import { useState, useEffect } from 'react';
import { MdOutlineNotificationsNone } from "react-icons/md";
import Link from 'next/link';
import Image from 'next/image'
import { useRouter, usePathname } from 'next/navigation';
import { BiMenu } from 'react-icons/bi';
import { useTopBar } from "./TopBarContext";

const FooterNavbar = ({ children, sidebarContent, userName }) => {
    const currentUrl = usePathname()
    const [subContent,setSubContent] = useState(null);
    const { topBarContent, triggerTopBarEvent } = useTopBar();

    useEffect(()=>{
      if(currentUrl.startsWith('/user/my-profile')){
        setSubContent(sidebarContent[1].subMenu)
      }else{
        setSubContent(null)
      }
    },[currentUrl])

    return(
        <div className="w-full h-full">
            <div className="w-full h-[3rem] flex justify-end items-center border-b-1 sticky top-0 bg-white z-[101]">
            {topBarContent == 'Filter'?<span className="w-full px-2 flex justify-between">
                        <span>Filters</span>
                        <button className='active:text-ornateorange' onTouchStart={()=>triggerTopBarEvent('Clear Filters')} 
                        onClick={()=>triggerTopBarEvent('Clear Filters')}
                        >Clear Filters</button></span>:<><Badge count={1} size='small' className='mr-[1.5rem]'><MdOutlineNotificationsNone className="text-gray3 cursor-pointer" size={20}/></Badge>
                        <div className="h-[2rem] w-[2rem] rounded-full border-1 mr-[1.5rem]"></div></>}                
             </div>
            {/* children pages */}
            <section className={` ${topBarContent !='Filter'?` ${subContent?'h-[calc(100vh-10rem)]':'h-[calc(100vh-7rem)]'}`:'h-[calc(100vh-3rem)]'} overflow-scroll`}>
                    {children}</section>

            {subContent && topBarContent !='Filter'  ?<div className="w-full flex items-center h-[3rem] border-t-1 sticky bottom-[4rem] bg-white z-[101] py-2">
            {subContent.map((menuItem) => {
              return (
                  <div className='flex-1 flex justify-center mx-1'>
                    <Link
                      className={`relative gap-1  py-2 items-center flex flex-col justify-center text-sm
                      ${(currentUrl == menuItem.path )?'font-medium text-violet':''}  w-[98%] cursor-pointer`}
                      href={
                        menuItem.path
                      }
                      onClick={()=>{menuItem.name ==='My Profile'?setSubContent(sidebarContent[1].subMenu):{}}}
                    >
                      <span
                      >
                        {menuItem.name}
                      </span>
                    </Link></div>
              )
            })}
            </div>:null}
            
            {topBarContent !='Filter' ?<div className="w-full flex items-center h-[4rem] border-t-1 sticky bottom-0 bg-white z-[101]">
            {sidebarContent.map((menuItem) => {
              return (
                  <div className='flex-1 h-full flex justify-center '>
                    <Link
                      className={`relative px-1 h-full items-center flex flex-col justify-center text-xs
                      ${(currentUrl == menuItem.path || 
                        (currentUrl.split('/')[2] == 'my-profile' && menuItem.path.split('/')[2] == 'my-profile'))?'font-medium bg-lightviolet text-violet':''}  w-[98%] cursor-pointer`}
                      href={
                        menuItem.path
                      }
                      onClick={()=>{menuItem.name ==='My Profile'?setSubContent(sidebarContent[1].subMenu):{}}}
                    >
                     <span className='text-l'>
                      <Image alt={menuItem.name} src={currentUrl == menuItem.path?menuItem.selectedTabIcon:menuItem.icon} width={25} height={25}
                      /></span>
                      <span
                      >
                        {menuItem.name}
                      </span>
                    </Link>
                    </div>
              )
            })}
            </div>:null}

         

        </div>
    )

}

export default FooterNavbar;