import { message, Upload } from 'antd';
import Image from 'next/image';
const { Dragger } = Upload;

const DragNDrop = () =>{
    const props = {
        name: 'file',
        // multiple: false,
        // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        beforeUpload: (file) => {
            const isPNG = file.type === 'image/png' || file.type == 'image/jpeg';
            if (!isPNG) {
              message.error(`${file.name} is not an image file`);
            }
            return isPNG || Upload.LIST_IGNORE;
          },
        onChange(info) {
          const { status } = info.file;
          if (status !== 'uploading') {
            console.log(info.file, info.fileList);
          }
          if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
          } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
        onDrop(e) {
          console.log('Dropped files', e.dataTransfer.files);
        },
      }

      return(
        <Dragger {...props}>
    <p className="w-full flex justify-center">
      <Image alt="upload_icon" src="/upload.svg" width={50} height={50} />
    </p>
    <p className=" ant-upload-text text-[16px]">Drag and drop here<br/> or <br/> 
    <span className='text-softviolet font-semibold'>Browse Files</span></p>
  </Dragger>
      )

}

export default DragNDrop;
