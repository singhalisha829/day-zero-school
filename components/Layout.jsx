import { usePathname } from "next/navigation";
import { useMediaQuery } from "react-responsive";
import dynamic from "next/dynamic";
import { useState, useEffect } from "react";
import LocalStorageService from "@/services/LocalStorageHandler";

const Navbar = dynamic(() => import("./ComponentsForMobile/NavbarForMobile"), { ssr: false });
const PhoneNavbar = dynamic(() => import("./ComponentsForMobile/FooterNavbar"), { ssr: false });
const Sidebar = dynamic(() => import("./Sidebar"), { ssr: false });
const localStorage = LocalStorageService.getService();

export default function Layout({ children }) {
  const currentUrl = usePathname();
  const isMobile = useMediaQuery({ maxWidth: 767 }, undefined, (matches) => {
    // This callback is only executed on the client side
    return matches;
  });
  const [sidebarContent, setSidebarContent] = useState([]);
  const [userName, setUserName] = useState(null);

  const userSidebarContent = [
    { name: 'Jobs', path: '/', icon: '/business_center.svg', selectedTabIcon: '/sel-business_center.svg' },
    { name: 'My Profile', path: '/user/my-profile/personal-details', icon: '/account_circle.svg', selectedTabIcon: '/sel-account_circle.svg',
        subMenu: [
          { name: 'Personal Details', path: '/user/my-profile/personal-details' },
          { name: 'Education', path: '/user/my-profile/education'},
          { name: 'Skills', path: '/user/my-profile/skills'},
    ], },
    { name: 'Settings', path: '/user/settings', icon: '/settings.svg', selectedTabIcon: '/sel-settings.svg' },
    { name: 'Contact', path: '/user/contact', icon: '/contact_support.svg', selectedTabIcon: '/sel-contact_support.svg' },
  ];

  const adminSidebarContent = [
    {
      name: "Dashboard",
      path: "/admin",
      icon: "/dashboard.svg",
      selectedTabIcon: "/sel-dashboard.svg",
    },
    {
      name: "Jobs",
      path: "/admin/posted-jobs",
      icon: "/business_center.svg",
      selectedTabIcon: "/sel-business_center.svg",
    },
    {
      name: "Students",
      path: "/admin/students",
      icon: "/users.svg",
      selectedTabIcon: "/sel-users.svg",
    },
    {
      name: "Companies",
      path: "/admin/company",
      icon: "/apartment.svg",
      selectedTabIcon: "/sel-apartment.svg",
    },
    {
      name: "Inbox",
      path: "/admin/inbox",
      icon: "/mail.svg",
      selectedTabIcon: "/sel-mail.svg",
    },
    {
      name: "Settings",
      path: "/admin/email-template",
      icon: "/settings.svg",
      selectedTabIcon: "/sel-settings.svg",
    },
  ];

  useEffect(() => {
    const user = { name: "John Doe" };
    const userDetails = localStorage.getUser();
    if (userDetails && userDetails.user === "user") {
      setSidebarContent(userSidebarContent);
    } else if(userDetails) {
      setSidebarContent(adminSidebarContent);
    }
    if (user != null || user != undefined) {
      setUserName(user.name);
    }
  }, [currentUrl]);

  return (
    // in case of login page , dont display sidebar/topbar
    <>
      {currentUrl == "/login" ? (
        <main>{children}</main>
      ) : !isMobile ? (
        <Sidebar
          sidebarContent={sidebarContent}
          userName={userName}
        >{children}</Sidebar>
      ) : (
        <PhoneNavbar
          sidebarContent={sidebarContent}
          userName={userName}
        >{children}</PhoneNavbar>
      )}
    </>
  );
}
