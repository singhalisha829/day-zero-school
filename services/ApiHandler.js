import axios from 'axios'
import LocalStorageService from './LocalStorageHandler'
// import { MAIN ,URL} from "../services/constants";

const localStorage = LocalStorageService.getService()

//Constants
const URL = process.env.SERVER

export const loginInstance = axios.create({
  baseURL: URL,
})

export const axiosInstance = axios.create({
  baseURL: URL + 'api/',
})

export const axiosEmployeeInstance = axios.create({
  baseURL: URL + 'api/web/',
})

export const axiosInterviewerInstance = axios.create({
  baseURL: URL + 'api/employee-dropdown/',
})

export const axiosJobInstance = axios.create({
  baseURL: URL + 'api/recruitment/',
})

/*=========== Interceptor ================ */
axiosInstance.interceptors.request.use(
  (request) => {
    if (request.url === 'hrm/dev/user_application_form/') {
      return request
    } else if (
      request.url.startsWith('hrm/user/user_application_form/') ||
      request.url.startsWith('hrm/user/user-upload-document/')
    ) {
      return request
    } else if (
      request.url.startsWith('hrm/state/') ||
      request.url.startsWith('hrm/city/') ||
      request.url.startsWith('hrm/user/user_application_form/')
    ) {
      return request
    }
    request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    return request
  },
  (error) => {
    return Promise.reject(error)
  }
)

axiosEmployeeInstance.interceptors.request.use(
  (request) => {
    request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    return request
  },
  function (error) {
    return Promise.reject(error)
  }
)

axiosInterviewerInstance.interceptors.request.use(
  (request) => {
    request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    return request
  },
  function (error) {
    return Promise.reject(error)
  }
)

axiosJobInstance.interceptors.request.use(
  (request) => {
    // request.headers['Authorization'] = 'token ' + localStorage.getAccessToken()
    // return request

    if (request.url.startsWith('jobs')) {
      return request
    } else if (request.url.startsWith('post/job-candidate')) {
      return request
    } else {
      request.headers['Authorization'] =
        'token ' + localStorage.getAccessToken()
      return request
    }
  },
  function (error) {
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  function (response) {
    // PROCESS AUTHORIZATION TOKEN HERE
    const isBlob = response.data instanceof Blob
    if (!isBlob) {
      if (
        response.data.status.code === 401 ||
        response.data.status.code === 403
      ) {
        window.location.href = '/login'
      }
    }

    return response
  },
  //redirecting to login page on following error codes
  function (error) {
    return Promise.reject(error)
  }
)

axiosEmployeeInstance.interceptors.response.use(
  function (response) {
    // PROCESS AUTHORIZATION TOKEN HERE
    // console.log(response)
    if (
      response.data.status.code === 401 ||
      response.data.status.code === 403
    ) {
      window.location.href = '/login'
    }
    return response
  },
  //redirecting to login page on following error codes
  function (error) {
    return Promise.reject(error)
  }
)

axiosInterviewerInstance.interceptors.response.use(
  function (response) {
    // PROCESS AUTHORIZATION TOKEN HERE
    // console.log('here')
    // console.log(response)
    if (
      response.data.status.code === 401 ||
      response.data.status.code === 403
    ) {
      window.location.href = '/login'
    }
    return response
  },
  //redirecting to login page on following error codes
  function (error) {
    return Promise.reject(error)
  }
)

axiosJobInstance.interceptors.response.use(
  function (response) {
    // PROCESS AUTHORIZATION TOKEN HERE
    // console.log(response)
    // if (
    //   response.data.status.code === 401 ||
    //   response.data.status.code === 403
    // ) {
    //   window.location.href = '/login'
    // }
    return response
  },
  //redirecting to login page on following error codes
  function (error) {
    return Promise.reject(error)
  }
)
